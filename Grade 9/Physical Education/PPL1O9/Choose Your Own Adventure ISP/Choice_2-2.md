## Stephanie Wants To Break Up With Kevin

## Scenario

- Stephanie is unable to convince Kevin not to take the drugs. Stephanie realizes that she shouldn’t be with someone like Kevin. She decides to break up with Kevin that night.

<img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR0i1cAOz0BgTfAi38gKb3YRsxmYcXYbi7dED9uuyYiMKnFbD2Yiw" width="500">

## Choices

- How does Stephanie Break up?
- [Stephanie Makes A Big Scene About The Break Up](https://gitlab.com/magicalsoup/Highschool/blob/master/Grade%209/Physical%20Education/PPL1O9/Choose%20Your%20Own%20Adventure%20ISP/Choice_2-2-1.md)

- [Stephanie Calmly Breaks Up With Kevin](https://gitlab.com/magicalsoup/Highschool/blob/master/Grade%209/Physical%20Education/PPL1O9/Choose%20Your%20Own%20Adventure%20ISP/Choice_2-2-2.md)