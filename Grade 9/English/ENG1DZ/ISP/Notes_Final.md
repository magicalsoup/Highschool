
|Day|Tasks Done|Notes|
|:--|:---------|:----|
|May 07, 2019|Pages 1-27 Read|Very interesting, Chapter 5999 can be an important chapter.|
|May 08, 2019|Pages 27-40 Read|Some good plot.|
|May 09, 2019|Pages 40-60 Read|Fillers are awesome|
|May 11, 2019|Pages 60-81 Read|Intresting conflict scenes, chapter 6028 is a important chpater|
|May 16, 2019|Pages 81-100 Read|Very nice ending, Foreshadowing|
# Notes Day 1

## Vocabulary / Diction
- `Periphery`: The outer limits or edge of an area or object. A marginal or secondary position in, or part or aspect of, a group, subject, or sphere of activity.
- `Tentative`: Not certain or fixed; provisional. Done without confidence; hesitant.
- `Mellower (mellow)`: (of a person's character) softened or matured by age or experience. (especially of sound, taste, and color) pleasantly smooth or soft; free from harshness.
- `Berate`: scold or criticize (someone) angrily.
- `IM'd (informal)`: Past tense of instant message, basically sending someone a instant message or direct message.
- `Chastise`: rebuke or reprimand severely.
- `Rebuke`: Express sharp disapproval or criticism of (someone) because of their behavior or actions.
- `Exhilarated`: Very happy, animated, or elated.

## Notes
- Main protaginist, who is unknown, somehow has the ability to be in another person's body, for a day. 
- His ability has some traits:
    - Only people that are the same age as him, are the people that he *possess*.
    - He can access facts of the people he is in, but not feelings.
- The protaginist has been like this for the past 16 years, show evident in this line: 
    - > Sixteen years is a lot of time to practice. I don’t usually make mistakes.
- Background of the protaginist
    - He's hurt people with his unknown *power* before. 
    - > I’ve harmed people’s lives in the past, every time I slip up, it haunts me. So I try to be careful.
    - A loner? With interesting ideals. 
    - > I am a drifter, and as lonely as that can be, it is also remarkably freeing. I will never define myself in terms of anyone else. I will never feel the
pressure of peers or the burden of parental expectation. I can view everyone as pieces of a whole, and focus on the whole, not the pieces. I have
learned how to observe, far better than most people observe. I am not blinded by the past or motivated by the future. I focus on the present,
because that is where I am destined to live.
- He's now in Justin's body, talking to his girlfriend, Rhiannon, and is different than Justin, which is mean and instead is nice to her.

# Notes Day 2

## Vocabulary / Diction
- `Exude`: Discharge (moisture or a smell) slowly and steadily. (of a person) display (an emotion or quality) strongly and openly.
- `Reverend`: Used as a title or form of address to members of the clergy. A member of the clergy (Informal).
- `Cuticle`: A protective and waxy or hard layer covering the epidermis of a plant, invertebrate, or shell. The outer cellular layer of a hair.
- `Musing`: A period of reflection or thought.
- `Purty` (informal): nonstandard spelling of pretty, used to represent dialect speech.

## Notes
- Chapter 6003, protaganist plans to make moves on Rihianna, has fallen for her.
- Nathan Dalby found the protaganists email, is susipicious, potientally foreshawdowing a hunt for the protaganist.
- Protaganist reveals his name to be just `A`.
- Justin seems to actually care about Rihianna?
- Devil things started to rise.
- Seems the protagnist is overrun by his emotions of love, or is a quick person to take risks.
- > I don’t deliberate. I don’t weigh my options. I just type and hit send.
- The use of email throughout the poem is pretty interesting, the format of it.
- nice personification?
- > nerves are jangling with possibility.
- reptition of the devil is becoming a trend.
- More characterization as the progtagnist starts to realize that he is not the only one with the ability, but is he right though.
- > I knowI’m not the only one.
- More devil idea. Its probably part of the main plot.
- > James does not feel like he was possessed by the devil.
- More characterization, sense of remorse, as its the protagnist first time to spend more than one day in someone else's family
- > But what I really feel is goodbye. I am leaving here, leaving this family. It’s only been two days, but that’s twice what
 I’m used to. It’s just a hint—the smallest hint—of what it would be like to wake up in the same place every morning.
 I have to let that go.
- Just realized this book uses alot of POV, with many different identities and personalities shown through the prospective of the person that the protagnist is in, for example, this depressed girl.
- > Kelsea Cook’s mind is a dark place. Even before I open my eyes, I know this. Her mind is an unquiet one, words and thoughts and impulses
constantly crashing into each other. My own thoughts try to assert themselves within this noise. The body responds by breaking into a sweat. I try to
remain calm, but the body conspires against that, tries to drown me in distortion.
It is not usually this bad, first thing in the morning. If it’s this bad now, it must be pretty bad at all times.
- Metaphors plus more POV
- > Depression has been likened to both a black cloud and a black dog. For someone like Kelsea, the black cloud is the right metaphor. She is
surrounded by it, immersed within it, and there is no obvious way out. What she needs to do is try to contain it, get it into the form of the black dog. It
will still follow her around wherever she goes; it will always be there. But at least it will be separate, and will follow her lead.
- This book uses alot of POV for the reader to not only know the character the protagnist is in well, but to show a different aspect of society, through different lenses. 

# Notes Day 3

## Vocabulary / Diction
- `Paraphernalia`: Miscellaneous articles, especially the equipment needed for a particular activity. trappings associated with a particular institution or activity that are regarded as superfluous.
- `Acolyte`: A person assisting the celebrant in a religious service or procession. An assistant or follower.
- `Ire`: Anger
- `Pantomime`: Express or represent (something) by extravagant and exaggerated mime.

## Notes
- Rihiannon shows slight characterization, from the shy girl that is like a slave to Justin to a more open girl, as she starts to have more conversations with the protagnist.
- She also drifting away from Justin, as we do not see much of his dialogue for a long time.
- Character philosphy?
- > And the only way to show her how it makes sense, the only way to make the enormity real, is for me to lean over and kiss her. Like last time, but
not at all like last time. Not our first kiss, but also our first kiss. My lips feel different against hers, our bodies fit differently. And there is also
something else that surrounds us, the black cloud as well as the enormity. I am not kissing her because Iwant to, and I am not kissing her because I
need to—I am kissing her for a reason that transcends want and need, that feels elemental to our existence, a molecular component on which our
universe will be built. It is not our first kiss, but it’s the first kiss where she knows me, and that makes it more of a first kiss than the first kiss ever
was.
- Its pretty obvious, but with characters that are more important, more pages and information are being given to, this depressed girl got around 5-10 pages of that chapter, as some got like 1 page or a half.
- Nathan's story got bigger.
- > THE DEVIL AMONGUS!
- Metaphors
- > From the moment we hit Annapolis, Austin is in his element.
- Idea of homosexuality and evil? I mean, its natural for it to be in an a anti-LGBTQ campaign, but the author decided to use this one, as it has `devil` in it, which leads back to the repeated idea of devil throughout the book when the protaganist met Nathan.
- > HOMOSEXUALITY IS THE DEVIL’S WORK,
- Nice sedusive, lovely atmosphere, especially when described by using dialogue.
- > “Maybe we should do something,” I suggest. <br> “Yeah. Sure.” <br> “Maybe just the two of us.” <br> Click. He finally gets it. <br> I move in. Touch his hand. Say, “I think that would be fun.”
- Justin actually shows signs that he cares about Rhinannon, possible characterization from the first mean and jerky Justin we got to know in the beginining. Also similes.
- > “Why not?” I ask. <br> He looks at me like I’m a complete idiot. <br> “Why not?” he says. “How about Rhiannon? Jeez.”
- More devil stuff. At this point, I think the author could be saying a theme of how humans will start to blame something else when they see something that they don't know how to explain.
- > WILLIAM CARLOS WILLIAMS TO REVEREND POOLE: ‘THE DEVIL MADE ME EAT THE PLUM.’
- Chapter 6008 has a bunch of devil words, it seems the `devil` is a strong plot/idea in this book. The protagnist also has a self-self conflict in this scene, where he starts to believe that he is the devil.
- Nice metaphor?
- > school day is already in full swing,
- Repeated words show that the protagnist is obessed with Rihannion, completly in love with her:
- > Nothing from her. <br> Nothing. <br> Nothing.

# Notes Day 4

## Vocabulary / Diction
- `Profusely`: To a great degree; in large amounts.
- `Debonair`: (of a man) confident, stylish, and charming.

## Notes
- Day 6023, The protaginist seems to have an affection to people that know who they are, i.e. people that are a different gender than their biological one.
- Also a common theme to show the lovestruck protagnist is that he keeps saying how far he is away from Rihiannon at the start of almost all the chapters since he met her.
- > I'am forty minutes away from her.
- More devil and reverand poole:
- > IF YOU BELIEVE THE DEVIL IS WITHIN YOU, <br> CLICK HERE OR CALL THIS NUMBER.
- Chapter 6015 It seems that the repeated idea above shows the conlusion, when he ends up in her very body.
- > I wake up, and I’m not four hours away from her, or one hour, or even fifteen minutes. <br> No, Iwake up in her house. <br> In her room. <br> In her body.
- There is a contrast to when the protagnist met Nathan, where it was just a simple love story turned dark rather quickly
- In chapter 6021, it is also a long chapter on a drunk person, who killed her brother, looks like the author is stressing a point or something.
- Major conflict between Justin, and A (in another person's body), and Justin calls Rhiannon a `slut` (diction).
- This line is very interesting. It shows that Rhiannon is quick to say no about A, but is hesitant to say the same thing to Justin, this is shown with the exclmation mark and the period. Also it characterizes Rhiannon, who doesn't love Jusin anymore.
- Also I guess also a character vs character conflict between Rhiannon and A.
- > “I don’t love him!” Rhiannon yells back. “But I don’t love you, either.”

# Notes Day 5

## Notes
- In chapter 6023, it is another long chapter about a transgendered person.
- In chapter 604, its is also a long chpater about someone's death.
- Throughtout the book, I feel like the author is showing as a different side of society, the dark side.
- More characterization of A
- > I try to pretend this is my life. I try to pretend these are my parents. But it all feels hollow, because I know better.
- Chapter: 6028, where A meets Nathan and the pope. Very nice atmosphere, the kind where on the outside, its all sunshine and rainbows, but on the inside is just evil. Great contrast also.
- Great use of tension, character v.s character.
- > “Get off of me,” I say, standing up. <br> He seems amused. “I’m not touching you. I am sitting here, having a conversation.” <br> “Get off of me!” I say louder, and start ripping at my own shirt, sending the buttons flying. <br> “What—” <br> “GET OFF OF ME!”
- Strong metaphor, that shows the real evil of the pope. I also assiociate imagery of red eyes, as murder links to blood, which links to red eyes.
- > Poole standing now with murder in his eyes.
- Theme? Foreshadowing?
- > “There’s no point in running away!” Poole yells. “You’re only going to want to find me later! All the others have!”
<br> Trembling, I turn up the radio, and drown him out with the sound of the song, and the sound of me driving away.
- Chapter 6033, which shows the final interaction between A and Rhiannon. A lovely atmosphere, with many allusions to common lovers in society. Heartbreak ending.
- > “I want to fall asleep next to you,” I whisper. <br>  “I love you,” I tell her. “Like I’ve never loved anyone before.” <br> Times moves on. The universe stretches out. I take a Post-it of a heart and move it from my body to hers. I see it sitting there.
I close my eyes. I say goodbye. I fall asleep.
- Final characterization: last line can mean alot of things.
- >I wake up two hours away, in the body of a girl named Katie.
Katie doesn’t know it, but today she’s going far away from here. It will be a total disruption to her routine, a complete twist in the way her life is
supposed to go. But she has the luxury of time to smooth it out. Over the course of her life, this day will be a slight, barely noticeable aberration.
But for me, it is the change of the tide. For me, it is the start of a present that has both a past and a future.
For the first time in my life, I run.
- I like how the author uses days as his chapters, 6000 / (365) = 16...., so it indirectly tells the reader how old the protagnist is.
- Also in this form, its like a diary, which also symoblizes the past, in my opinoin, he's telling us his stories from the future.