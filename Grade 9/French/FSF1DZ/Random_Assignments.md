# April 26 2019

## Page 135, Ex #5
1. Quel jour est-ce que Beatrice est arrivee a Paris?
    - mecredi 18 juin
2. Quel jour est-ce qu'elle est partie de paris?
    - samedi 21 juin
3. Combien de temps est-elle restee?
    - trois temps
4. Comment est-elle arrivee a Paris?
    - L'avion
5. Commest est-elle partie?
    - L'avion
6. Dans quel gitek est ekke restee?
    - Hotel Esmeralda
7. Quel jour est-ce qu'elle est montee a la Tour Eiffel?
    - Vendredi 20 juin
8. Quel jour est-ce qu'elle a visite le Louvre?
    - Mercredi 18 juin
    - Quel autre (other) musee a-t-elle visite
        - Musee Picasso
9. Quel jour est-ce qu'elle a fait des achats?
    - Vendredi 20 juin
    - Ou est-elle allee?
        - Bon Marche
    - Qu'est-ce qu'elle a achete?
        - Pantalon, 2 chemises (60 euros)
10. Ou a-t-elle dine le premier jour?
    - Chez Claudine's
11. Avec qui a-t-elle dine le deuxieme jour?
    - diner avec Marc
12. Quand est-ce qu'elle a dine avec Marc?
    - Dans Le soir
    - Qu'est-ce qu'ils ont fait apres?
        - elle est allee le discotheque

## Page 162 

### Le Menu
1. Combien d'omelettes differentes  a-t-il au menu?
   - 10
2. Parmi (Among) ces omelettes, quelle est votre omelette favorite?
    - Omelette champignons et fromage
    - Combien est-ce qu'elle coute?
        - $6.50 
3. Qu'est ce qu'il y a dans une omelette espagnole?
    - tomates, pelees, poivrons et oignons
4. Dans quelles omelettes est-ce  qu'il y a des tomates, des pommes de terre? des oignons?
    - Omelette espagnole, Omelette nicoise, Omelette provencale
    - Omelette paysanne, Omelette western
    - Omelette espagnole, Omelette lyonnaise, Omelette paysanne, Omelette western

### Les salades et les plats divers
1. Allez-vous prendre une salade ou une soupe?
    - une salade
    - Qu'est-ce que vous avea choisi
        - Salade Cesar
2. Comment dit on hamburger a Quebec>
    - Hambourgeois
3. Qu'est-ce que vous allez choisir si vous avez tres faim?
    - Hambourgeois deluxe
4. Qu'est-ce que vous allez commande si vous n'avez pas tres faim?
    - Salade Cesar

### Les crepes et les desserts
1. Comment dit-on glace a Quebec?
    - creme glacee
2. Chosisissez une crepe. Qu'est-ce qu'il y a dans cette crepe?
    - Fraises et creme glacee
3. Quel dessert avez-vous choisi? Quel est son ingredient principal?
    - Mousse au chocalat
        - Mousse

### Les boissons
1. Comment dit-on boisson a Quebec? Comment dit-on soda?
    - Breuvages
    - Liqueurs douces
2. Qu'est-ce que vous avez choisi comme boisson
    - Liqueurs douces

### UN REPAS COMPLET
1. Imaginez que vous allez prendre un repas complet
    - Vous voulez depenser seulement 10 dollars. Choisissez un plat prinicipal un dessert et une boisson
        - Qu'est-ce que vous avez choisi
            - Hambourgeois deluxe + Liqueurs douces
        - Combien coute votre repas?
            - (6.75 + 1.95) * 1.15 = $10.01 = $10.00
    - Vous avez tres faim. Choisissez un plat froid, un plat chaud, un dessert et une boisson
        - Qu'est-ce que vous avez choisi
            - Hambourgeois deluxe, Fraises et creme glacee crepe, Liqueurs douces
        - Combien coute votre repas?
            - (5.50 + 6.75 + 1.95) * 1.15 = $16.35
        
2. Avec un(e) camarde qui va jouer le serveur (la serveuse), composez et jouez un dialogue ou vous commandez votre repas

