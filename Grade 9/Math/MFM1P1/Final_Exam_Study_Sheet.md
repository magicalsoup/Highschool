# Study Sheet

# Unit 1: Diagnostic & Essential Number Skills
## Rounding and Decimals

### Decimals
- #### Terms:
    - Given the number `9123.456`:
        - The **`tenth`** is the `4`.
        - The **`hundredth`** is `5`.
        - The **`thousandths`** is `6`.
        - The **`ones`** is `3`.
        - The **`tens`** is `5`.
        - The **`hundreds`** is `1`.
        - The **`thousands`** is `9`.
        - **Remember, `tens` and `tenths` may sound the same, but they are `DIFFERENT`**!
        - 
- To round to a **`tenth`**, **`hundredth`**, and **`thousandths`**
    - Tenths
        - If the `hundredth` is `5` or higher, round up, else, round down.
        - Example:
            - Round `12.53223` to the tenths
            - The answer is `12.5`, as the hundredths, or `3` is smaller than 5.
    - Hundredth
        - If the `thousandth` is `5` or higher, round up, else, round down.
        - Example:
            - Round `12.53521` to the hundredth
            - The answer is `12.4`, as the thousandths, or `5` is bigger or equal to 5. 
    - Thousandth
        - If the number of the `thousandth` is `5` or higher, round up, else, round down.        
        - Example:
            - Round `12.5356` to the thousandths
            - The answer is `12.536`, as the number after the thousandths, or `6` is bigger than 5.

         
        
- To round to a **`ones`**, **`tens`**, **`hundreds`**, and **`thousands`**
    - Ones
        - If the `tenths` is `5` or higher, round up, else, round down. 
        - Example:
            - Round `123.5333` to the ones
            - The answer is `124`, as the tenths, or `5` is bigger than or equal to 5.
    - Tens
        - If the `ones` is `5` or higher, round up, else, round down.
        - Example:
            - Round `123.5777` to the tens
            - The answer is `120`, as the ones, or `3` is smaller than or equal to 5.
    - Hundreds
        - If the `tens` is `5` or higher, round up, else, round down.
        - Example:
            - Round `177.34343` to the hundreds
            - The answer is `200`, as the tens, or `7` is bigger than 5.
    - Thousands
        - If the `hundreds` is `5` or higher, round up, else round down.
        - Example:
            - Round 566.777` to the thousands
            - The answer is `1000`, as the hundreds, or `5` is bigger or equal to 5.

## Integers

### Multiplication and Division
- Pretend `a` and `b` are random positive numbers

  |Type|Outcome|
  |:---|:------|
  |a &times; b|Positive number|
  |a &times; (-b)|Negative number|
  |(-a) &times; b|Negative number|
  |(-a) &times; (-b)|Positive number|
  |a &div; b|Positive number|
  |a &div; (-b)|Negative number|
  |(-a) &div; b|Negaitve number|
  |(-a) &div; (-b)|Positive number|
  
- Treat as normal divion and multiplacation, and just add the negative sign infront of the number according to the rules above.


- Practice
    - 8 &times; -7
        - Answer: `-56` 
    - 2 &times; 4
        - Answer: `8`
    - -7 &times; -7
        - Answer: `1`
    - -10 &times; 4 
        - Answer: `-40`
    - 8 &div; 4
        - Answer: `2`
    - -16 &div; -8 
        - Answer: `2`
    - -4 &div; 1
        - Answer `-4`
    - 9 &div; -3 
        - Answer: `-3` 

### Addition and Division

- Pretend `a` and `b` are random postive numbers.
 
    | Type | Equivalent |
    |:-----|:-----------| 
    | a + b | a + b | 
    | b + a| b + a |
    |a+(-b) | a - b |
    |(-a)+b|b-a|
    |a-b|a-b|
    |b-a|b-a|
    |a-(-b)|a+b|
    |(-a)-b|-(a+b)|


### Order Of Operation
- BEDMAS
-  Follow ```BEDMAS``` for order of operations if there are more than one operation   

 | Letter | Meaning |   
 |:------:|:-------:|   
 | B / P | Bracket / Parentheses |    
 | E | Exponent |   
 | D | Divison |   
 | M | Multiplication |   
 | A | Addition |   
 | S | Subtraction |    

- <img src="https://ecdn.teacherspayteachers.com/thumbitem/Order-of-Operations-PEMDAS-Poster-3032619-1500876016/original-3032619-1.jpg" width="300">
- Follow order of operation, inorder to do know which operation to do first.
- Example: Given $`(2+4) \times 5 - 9 \div 3`$
    - First do everything in brackets: $`(6) \times 5 - 9 \div 3`$
    - Then do multiplication/division: $`30 - 3`$
    - Then finally, do subtaction/addition: $`27`$
    - The answer is `27`.

## Fractions / Rational Numbers
- The number on the top is called the `numerator`.
- The number on the bottom is called the `denominator`.
- A fraction in its most simple form is when the `numerator` and `denominator` cannot be both divided by the same number.

### Additions / Subtractions With Fractions
- Example: $`\frac{3}{5} + \frac{4}{3}`$
- Find `common denominator`, which is `15`, as `5` and `3` both are factors of `15`.
    - You can do this easily with a table, just count by the number you are using, for example:
    - |Counting by 5s | Counting by 3s |
      |:--|:--|
      |5|3|
      |10|6|
      |15|9|
      |20|12|
      |25|15|
    - As you can see, both columns contain the number `15`, so `15` is the common denominator.
    - Now, after we find the denominator, we must convert the fraction so that it has the `common denominator`. To do this, we must multiply the denominator by a number, so that it equals the `common denominator`. For the first fracion $`\frac{3}{5}`$, the `denominator` is `5`, to get to `15`, we must multiply it by `3`. Now, whatever we do on the bottom, me **MUST** do it on the top too, so we also multiply the `numerator` by `3` as well, the new fraction is now $`\frac{3 \times 3}{5 \times 3} = \frac{9}{15}`$.
    - We now do the same thing to the other fraction: $`\frac{4 \times 5}{3 \times 5} = \frac{20}{15}`$
    - Now that the denominators are the same and the fractions are converted, we can just simply add the `numerators` together while keeping the `denominator` the same. The result is $`\frac{9 + 20}{15} = \frac{29}{15}`$.
    - The same steps applied to subtracion, with the only difference of subtacting the numerators rather than adding them.

### Multiplaction With Fractions
- To multiply a fracion, simply multiply the `numerators` together, and the `denominators` together.
- Example: $`\frac{3}{6} \times \frac{7}{4}`$
    - Answer: $`\frac{3 \times 7}{6 \times 4} = \frac{21}{24}`$

### Division With Fractions
- To divide 2 fractions, flip the second fraction upside down and multiply them togehter.
- Or, in advanced terms, mulitply the first fraction by the reciporocal of the second fraction.
- Given an example: $`\frac{4}{2} \div \frac{6}{9}`$
    - First, flip the second fraction upside down: $`\frac{4}{2} \div \frac{9}{6}`$
    - Then change the division to a multiply: $`\frac{4}{2} \times \frac{9}{6}`$
    - Then multiply the 2 fractions $`\frac{4 \times 9}{2 \times 6} = \frac{36}{12}`$

# Unit 2: Measurement

## Measuring Perimeter and Area

### 2D Shapes
 |Shape|Formula|Picture|
 |:----|:------|:------|
 |Rectangle|```Area```: $`lw`$ <br> ```Perimeter```: $`2(l+w)`$|<img src="https://lh5.googleusercontent.com/Ib1Evz5PUwd4PzRmFkHj9IY2Is-UthHoUyyiEHAzkJP-296jZvMmHJM1Kws4PmuTeYHV2ZBIJenc4W1pKtsSHvU82lyjOed2XKBb1PWnoaeJ3sSPuaJgSTg8JWbxrvplabCanvTD" width="200">|   
 |Triangle|```Area```: $`\frac{bh}{2}`$ <br> ```Perimeter```: $`a+b+c`$|<img src="https://lh6.googleusercontent.com/covvHwXxQhrK2Hr0YZoivPkHodgstVUpAQcjpg8sIKU25iquSHrRd2EJT64iWLsg_75WnBw4T9P0OTBiZDkpqEkXxflZQrL16sNhcFfet_z4Mw5EPFgdx_4HzsagV0Sm5jN6EKr_" width="200">|
 |Circle|```Area```: $`πr^2`$ <br> ```Circumference```: $`2πr`$ or $`πd`$|<img src="https://lh5.googleusercontent.com/RydffLVrOKuXPDXO0WGPpb93R8Ucm27qaQXuxNy_fdEcLmuGZH4eYc1ILNmLEx8_EYrRuOuxFavtL9DF1lTWYOx9WaYauVlu0o_UR6eZLeGewGjFNUQSK8ie4eTm1BMHfRoQWHob" width="200">|
 |Trapezoid|```Area```: $` \frac{(a+b)h}{2}`$ <br> ```Perimeter```: $`a+b+c+d`$|<img src="https://lh6.googleusercontent.com/_nceVtFlScBbup6-sPMulUTV3MMKu1nonei0D1WY-KRkpHSbPCIWgDO8UGDQBGKh8i0dkAqOhFUHl7YHCFOt6AMRSJiXALlBBY0mBo1MMZxHRVcg8DknSlv4ng7_QswcZtaRwrJb" width="200">|

## Right Triangle and Pythagorean Theorem

- `a` and `b` are the two legs of the triangle or two sides that form a 90 degree angle of the triangle, `c` is the hypotenuse        
- $`a^2+b^2=c^2`$     

- <img src="http://www.justscience.in/wp-content/uploads/2017/05/Pythagorean-Theorem.jpeg" width="400">

## Area of Composite Figures
- You can cut any normal polygon into standard polygons you know, for example, a polygon shaped as a house can be split into a recntalge and a triangle. All normal polygon, can be simplified into triangles.

## Perimeter of Composite Figures
- Same as an area of composite figures, cut them down into much simplified shapes so you can easily calculate its area. Alternatively, if its possible, you can even measure the shape using a tool such as ruler.

### 3D Objects
|3D Object|Formula|Picture|
 |:----|:------|:------|
 |Rectangular Prism|```Volume```: $`lwh`$ <br> ```SA```: $`2(lw+lh+wh)`$|<img src="https://lh6.googleusercontent.com/-mqEJ4AMk3xDPfqH5kdVukhtCGl3fgTy2ojyAArla54c7UoAnqKW_bsYSaFySXLplE59pqLIg5ANZAL1f6UEejsrKJwQCfyO7gwUQmSDoJJtQG_WkfHcOFDjidXV4Y4jfU2iA5b-" width="200">|
 |Square Based Pyramid|```Volume```: $`\frac{1}{3} b^2 h`$ <br> ```SA```: $`2bs+b^2`$|<img src="https://lh5.googleusercontent.com/iqaaJtx-Kx4vFT3Yp6YLOmpDFL7_qk2uh0Z21pgPJMDRgchiUBcHeTWkMrR9mFjxCj8w7za1xwN9bo4UFACPZRMSl-V67uPv9FvDyNJVjedmeehx5K-iUK9sBhObhNsLJpNItkg0" width="200">|
 |Sphere|```Volume```: $`\frac{4}{3}  πr^3`$ <br> ```SA```: $`4πr^2`$|<img src="https://lh6.googleusercontent.com/DL6ViJLbap2dcSAlZnYKR7c33033g8WuJVvqz0KpzCyIJ0wXyrh5ejoLhrTxlX9uASQlxPmihm8doU1sNbaQxqBcTaPnP-lC6LUrPqzPNi11AHiHQAu3ag7uIGcwzkdC9e5uo1en" width="200">|
 |Cone|```Volume```: $` \frac{1}{3} πr^2  h`$ <br> ```SA```: $`πrs+πr^2`$|<img src="https://lh5.googleusercontent.com/V3iZzX8ARcipdJiPPFYso_il3v_tcrYHZlFnq3qkekRSVBVcj8OzWxMuBqN45aHbv6y-fDH4uY11Gus3KMrvf_Z_TvsfJCwZZ19Ezf7Yj6DzVirp-Gx3V0Qy793ooUwTDmdKW_xq" width="200">|
 |Cylinder|```Volume```: $`πr^2h`$ <br> ```SA```: $`2πr^2+2πh`$|<img src="https://lh5.googleusercontent.com/4uWukD3oNUYBG-fLX2-g58X8at0h74al7BJI5l78LZ0Bu9nXuZnt9dp9xiETeLTqykP-WWFdO_H5by4RkgDVxSENZgootSrAsOUoY2RWubflNOAau1bVFgm9YIe59SmiFlyxwgDV" width="200">|
 |Triangular Prism|```Volume```: $`ah+bh+ch+bl`$ <br> ```SA```: $` \frac{1}{2} blh`$|<img src="https://lh3.googleusercontent.com/_oRUVgfdksfUXGKQk3AtrtY70E8jEq-RRK-lB9yKc_Rtio2f2utGAY-rI4UqjWEeTzUoN_r7EiqdZZeeE12EY-fiV55QQKdvnv4y4VaxQ9xt9Izugp6Ox_LqIUpQzPKVldptgKWm" width="200">|

# Unit 3: Optimization

## Optimization (For Maximimizing Area/Volume, or Minimizing Perimeter/Surface Area)

### 2D Shapes

 |Shape|Maximum Area|Minimum Perimeter|
 |:----|:-----------|:----------------|
 |4-sided rectangle|A rectangle must be a square to maximaze the area for a given perimeter. The length is equal to the width<br>$`A = lw`$<br>$`A_{max} = (w)(w)`$<br>$`A_{max} = w^2`$|A rectangle must be a square to minimaze the perimeter for a given area. The length is equal to the width.<br>$`P = 2(l+w)`$<br>$`P_{min} = 2(w)(w)`$<br>$`P_{min} = 2(2w)`$<br>$`P_{min} = 4w`$|
 |3-sided rectangle|$`l = 2w`$<br>$`A = lw`$<br>$`A_{max} = 2w(w)`$<br>$`A_{max} = 2w^2`$|$`l = 2w`$<br>$`P = l+2w`$<br>$`P_{min} = 2w+2w`$<br>$`P_{min} = 4w`$|

# Unit 4: Relationship in Geometry

## Angles

|Angle|Description|Example|
|:----|:----------|:------|
|Acute Angle|Less than 90 degrees|<img src="https://www.mathsisfun.com/geometry/images/acute-angle.svg" width="100">|
|Right Angle|90 degrees|<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/6c/Right_angle.svg/1200px-Right_angle.svg.png" width="100">|
|Obtuse Angle|More than 90 degrees|<img src="https://www.chilimathwords.com/wp-content/uploads/2018/05/120-degree-obtuse-angle.png" width="200">|
|Straight Angle|180 degrees|<img src="https://www.chilimathwords.com/wp-content/uploads/2018/05/180-degree-straight-angle.png" width="200">|
|Reflex Angle|More than 180 degrees|<img src="https://www.mathematics-monster.com/images5/reflex_angle_q4.jpg" width="200">|


## Angle Theorems

### 1. Transversal Parallel Line Theorems (TPT)     
   a. Alternate Angles are Equal ```(Z-Pattern)```   
   b. Corresponding Angles Equal ```(F-Pattern)```    
   c. Interior Angles add up to 180 ```(C-Pattern)```      

  - <img src="https://dj1hlxw0wr920.cloudfront.net/userfiles/wyzfiles/58a52a99-05da-4595-88b8-2cbca91e8bbf.gif" width="300">   

### 2. Supplementary Angle Triangle (SAT)     
  - When two angles add up to 180 degrees     

  - <img src="https://embedwistia-a.akamaihd.net/deliveries/cdd1e2ebe803fc21144cfd933984eafe2c0fb935.jpg?image_crop_resized=960x600" width="500">   

### 3. Opposite Angle Theorem (OAT) (OAT)    
  - Two lines intersect, two angles form opposite. They have equal measures    

  - <img src="https://images.slideplayer.com/18/6174952/slides/slide_2.jpg" width="400">   

### 4. Complementary Angle Theorem (CAT)    
  - The sum of two angles that add up to 90 degrees     

  - <img src="https://images.tutorvista.com/cms/images/67/complementary-angle.png" width="300">   

### 5. Angle Sum of a Triangle Theorem (ASTT)   
  - The sum of the three interior angles of any triangle is 180 degrees    

  - <img src="https://dj1hlxw0wr920.cloudfront.net/userfiles/wyzfiles/f0516fa1-669b-441d-9f11-a33907a2a0b0.gif" width="300">   

### 6. Exterior Angle Theorem (EAT)
  - The measure of an exterior angle is equal to the sum of the measures of the opposite interior angles      

  -<img src="https://www.katesmathlessons.com/uploads/1/6/1/0/1610286/exterior-angle-theorem-diagram-picture_orig.png" width="300">

### 7. Isosceles Triangle Theorem (ITT)   
  - The base angles in any isosceles triangle are equal   

  - <img src="http://www.assignmentpoint.com/wp-content/uploads/2016/06/isosceles-triangle-theorem.jpg" width="400">

### 8. Sum of The Interior Angle of a Polygon
  - The sum of the interioir angles of any polygon is ```180(n-2)``` or ```180n - 360```, where ```n``` is the number of sides of the polygon

  - <img src="https://i.ytimg.com/vi/tmRpwCM1K1o/maxresdefault.jpg" width="500">


### 9. Exterior Angles of a Convex Polygon
  - The sum of the exterior angle of any convex polygon is always ```360 degrees```   

  - <img src="https://image.slidesharecdn.com/findanglemeasuresinapolygon-110307143453-phpapp02/95/find-angle-measures-in-a-polygon-11-728.jpg?cb=1299508555" width="400">

# Unit 5: Proportional Reasoning

## Equivalent Ratios
- An easy way to see if ratios are equivalent is to see if the ratios are the same in its simpliest form. A ratio is a fraction in essence, the first number is the `numerator`, and the second number is the `demominator`.

- An easy way to find an equivalent ratio is just to multiply the everything by 2, for example, given a ratio `2:5`, multiply everything by 2, so your new ratio will be `2x2:5x2 = 4:10`.

## Ratio and Proportions
- Ratios link directly with proportions, as a ratio of one thing can be applied to another ratio of another thing. 
- Example
    - Given one ratio of one triangle's height to base and another ratio of another triangle's heigth to base, we can find one missing side length given that we have the other 3.
    - Lets say the 2 ratios are `1:5` and `3:x`, given that `x` is a random number.
    - To solve `x`, we can cross-multiply, or multipling the `numerators` with the `denominators` and setting them equal to each other. Thus, we can first make the ratios into fractions and then do: $`\frac{1}{5} \text{ cross-multiply } \frac{3}{x} \rightarrow 1x = 3 \times 5`$. Which we can then solve for `x`, which is `15`.
    

## Solving Algebraic Equations
- To solve equations, just remove the extra bits on both sides by doing the opposite. So if its addition, do subraction to get rid of the added numbers, and vice versa:

|Example|To solve:|
|:------|:--------|
|$`x + 3 = 15`$|$`(x + 3)-3 = 15-3`$ <br> $`x = 12`$|
|$`x - 3 = 15`$|$`(x - 3)+3 = 15+3`$ <br> $`x = 18`$|
|$`x \div 3 = 15`$|$`(\frac{x}{3}) \times 3 = 15 \times 3`$ <br> $`x = 45`$|
|$`x \times 3 = 15`$|$`\frac{x \times 3}{3} = \frac{15}{3}`$ <br> $`x = 5`$|


## Fraction to Decimal
- Simply divide the `numerator` by the `denominator`. So $`\frac{3}{4} = 3 \div 4, \text{or } 0.75`$.
## Fraction to Percent
- The percentage symbol `%`, means the number has been multiplied by 100, so given a fraction $`\frac{3}{4}`$, convert the fraction into a decimal. Then multiply that decimal by `100`.
- $`\frac{3}{4} = 0.75 \rightarrow 0.75 \times 100 = 75\%`$

## Percent as a Ratio
- Simply divide the first number by the second number, and multiply by one `100`. It is the same as making the ratio into a fraction, then converting the fraction into a percentage.


# Unit 6: Graphing Relations

## Definitions
- `relation`: describes how one variable is connected to another.
- `Axis` (plural is axes): 
    - Vertical: used for **dependent** variable.
    - Horizontal: used for **independent** variable.
- `Variable`: a letter or symbol used to represent a quantity that changes
    - **Independent** variable is **NOT** controlled/affecet by another variable
    - **Dependent** variable is one that **IS** controlled/affected by the **independent** variable.
- `Trend`: a general direction or tendency
- `Line of best fit`: a **line** that passes as close as possible to a set of plotted points
    - a **correlation** describes how well one variable relates to another. Possible types of correltaion:
        - positive
        - negative
        - none
        - strong
        - weak
- `Curve of best fit`: a **curve** that passes as close as possible to as set of plotted points.
- `Interpolation`: Data **inside** the given data set range.   
- `Extrapolation`: Data **outside** the data set range. 

## Graphs:
- `Title`: Name given to a graph and placed above the graph.
- `Axis Label`: Axes are labeled with the scale, what was measured and its units.
- `Scale`: evenly spaced numbers which differ by an equal amount. Note: the scale may have a break at the beginning.



## Interpreting Scatter Plots
- A scatterplot graph is there to show the relation between two variables in a table of values.    
- A line can be drawn through the most concentrataed points, to show a trend.
- - Given a value, to find the other value, use follow the line that the dot is on and check the value on the other axis. For example, if a point is on `(1, 2)`, if you are given `2`, to get `1`, just following the vertical line to find the corresponding number on the `x-axis`.
- <img src="https://www.varsitytutors.com/assets/vt-hotmath-legacy/hotmath_help/topics/line%20of%20best%20fit-eyeball/lineofbestfit-e-1.gif" width="300">   

## Line Of Best Fit
- A line that represents the `trend` in a graph.
### How To Find The Line of Best Fit
1. Simply draw the line according to the properties of the line below.
2. Use a ruler when drawing the line.
3. Make sure the line of best fit represents the trend.
 
### Properties Of Line Of Best Fit
- Shows the trend for the data in a scatter plot.
- Shows the pattern and direction of the data points.
- Should pass through as many points as possible.
- Remaining points should be grouped equally above and below the line and spread out along the line.
- Helps make predictions for values not actually recorded and plotted.

## Curve Of Best Fit
- A curve that represents the `trend` in a graph.

### How To FInd The Curve Of Best Fit
1. Simply draw a curve that connects all the points.
2. You can also use an online graphing calculator, like [DESMOS](https://www.desmos.com/calculator).
    

## Time - Distance Graph
- Time is the independent variable and distance is the dependent variable   
- You can't go backwards on the x-axis, as you can't go back in time   
- Plot the points accordingly   
- Draw the lines accordingly   
- <img src="https://dryuc24b85zbr.cloudfront.net/tes/resources/6061038/image?width=500&height=500&version=1519313844425" width="400">   

**Direction is always referring to:**

 1. ```go towards home```    
 2. ```going away from home```   
 3. ```stop```    

## Graphing Linear Relations
- A linear relation is a `straight line`.

### Graphing Using Table Of Values
- To graph the relation, first look at the `Table of Values`.
- Then decide which columns are the x and y axis.
- Then do a scatter plot/graph, where the numbers are the points.
- Example:
    - Given the table of values:
    - |Hours Worked|Pay in dollars|
      |:--|:-----|
      |3|100|
      |6|200|
      |9|300|
      |12|400|
    - First label the x and y axis, since time is the **independent variable**, the `Hours Worked` will be `x-axis`. The `Pay in dollars` will be our y-axis.

## Graphing Non-Linear Relations
- Plot the points in a scatter plot.
- Use one smooth curve to connect all the points.


# Unit 7: Linear Relations
- Cartesian Coordination System: its a coordinate system that has 4 grids, with x and y values. 
- <img src="https://ourvirtualclass.edublogs.org/files/2017/03/5rWa4At7ZCqwqxM2qG1jgFfM-29qxgiw.png" width="300">
- `Direct Variation`: A line that passes through or from the `origin`. Simply put, there is a point where the x and y values both equal to 0.
- `Partial Variation`: A line that does not pass through or from the `origin`. Simply put, there isn't a point where the x and y values both equal to 0.

## Recognizing Linear Relations
- Use either `table of values` or a graph to determine it.
- If the `first differences` in the table of value is constant, it is a linear relation. 
- If the line that connects all the points is a straight line, it is a linear relation.

## Average Speed As Rate Of Change
- $` \text{average speed} = \frac{\text{Distance Traveled}}{\text{Time Taken}}`$
- The average speed, is simply equal to the rate of change.

## Solving Equations
- `Equation`: When the alegebraic expression are set to equal to each other.
- `Variable`: A number that can change (varies) $`\rightarrow`$ unknown.
- `Constant`: A number that always stays the same (doesn't vary).
- `To solve an equation is`: fidn the value of the variable (unknown), so that the left side of the equation and the right side of the equation are equal.

### Tips 
- Remember if you do something to one side, you must do it to the other.
- Make sure to flip the negative/positive sign when moving an value to the other side.

## Deteriming Values in a Linear Relation  

### Steps

1. Enter the data given in the question into this table.
2. Graph the data, Join the points with a line.
3. Determine the rate of change.
4. Extend the line on the graph to the left until it intersects the vertical axis.
5. Simply use the graph, by either looking at the x or y axis to solve the questions that are given.

## Two Linear Equations
- Plot the points and draw the lines for all the equations.
- The point where they intersect is called the point of intersection, and is when the equations equal to one another (the x and y values).
- In terms of money, the less steep the line, the better the deal is.

# Unit 8: Polynomials
- `like terms`: are variables that have the same name and are raised to the same power (eg. $`x^2 \text{and } 2x^2`$)
- `unlike terms`: are variables that have the same name and are not raised to the same power (eg $`x^2 \text{and } x`$).

## Summing Polynomials
1. If there are brackets, first simplify and expand them.
2. Simply collected the `like-terms` and simplify them.

- Eg. $`(2x^2+2x+3) + (7x + x^2 - 5)`$
- First you expand/open the brackets.
- $`= 2x^2 + 2x + 3 + 7x + x^2 - 5`$
- Then you collect the like terms and group them together.
- $`= 2x^2 + x^2 + 2x + 7x + 3 - 5`$
- Then you simplify.
- $`= 3x^2 + 9x - 2`$

## Subtracting Polynomials
- You Simply do the same thing as summing polynomials, except to you need to be careful and apply **distributive property** with the `-1` wherever neccessary.
- Eg. $`(4x^2 - 5) - (3 - x^2)`$
- First open the bracets.
- $`= 4x^2 - 5 - 3 + x^2`$
- Group like terms together.
- $`= 4x^2 + x^2 - 5 - 3`$
- Simplify
- $`= 5x^2 - 8`$

## Multiplying Polynomials With A Constant
- To do this, you simply apply the **distributive property**.
- Eg. $`-5(x^2 - 3x + 4)`$
- Apply distributive property.
- $`= -5(x^2) + 5(3x) -5(4)`$
- Then open the brackets by multiply the numbers together.
- $`= -5x^2 + 15x - 20`$

## Multiplying Polynomials With A Monomial.
- To do this, you also use **distributive property**
- Simply multiply everything in the polynomial by the monomial.
- Eg.$`4x(3x^2 + 5x - 3)`$
- Use distributive property and open the brackets.
- $`= 4x(3x^2) + 4x(5x) + 4x(-3)`$
- Then you reformat the numbers.
- $`= (4)(3)(x)(x)(x) + (4)(5)(x)(x) + (-3)(4)(x)`$
- And simplify.
- $` = 12(x^3) + 20(x^2) + -12(x)`$
- $` = 12x^3 + 20x^2 - 12x`$

## Multiplying A Monomial With A Monomial
- To do this, simply reformat the variables after multpilication (**distributive property)**, and simplify.
- Eg. $`4x(-12x)`$
- Use **distributive property** and reforat the numbers.
- $`= (4)(-12)(x)(x)`$
- Then you simplify.
- $`= (-48)(x^2)`$
- $`= -48x^2`$

## Solving Equation
- To solve a equation, is to find the **missing value** and make sure the left side and the right side are equal.
- Remember, to solve an equation, it usually requires **multiple** steps.
1. First simplify as much as you can.
2. Use **distributive property** and open brackets if there are any.
3. Regroup the terms.
4. Simplify Again (use **distributive property** whereever nescessary).
5. Check.


## Tips
- Watch out for negatives signs.
- Make sure to label your graph CORRECTLY, with the proper x and y axis.

## Credits
- Made by Magicalsoup(James)