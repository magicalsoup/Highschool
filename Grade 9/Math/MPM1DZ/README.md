# Math Study Sheet!!!!
- Due do performance, issues, I decieded to split the study sheet into the 6 units, hope that doesn't hinder anyone.

# Exam Detail
|Unit|Marks|
|:---|:----|
|Unit 1|10|
|Unit 2|10|
|Unit 3|9|
|Unit 4|11|
|Unit 5|11|
|Unit 6|8|
|Forms|4|
|Total|63|

|Section|Marks|
 |:------|:----|
 |Knowledge|21|
 |Application|23|
 |Thinking|12|
 |Communication|3|
 |Forms|4|

 |Part|Question|
 |:---|:-------|
 |A|9 multiple choice|
 |B|10 Short Answer --> <br>- 7 Knowledge questions<br>- 3 Application Questions|
 |C|10 Open Response --> <br>- 10 Knowledge Questions<br>- 5 Application Questions<br>- 3 Thinking Questions<br>- 1 Communication Question|


# General Tips
- Be sure to watch out for units, like ```cm``` or ```km```   
- Watch out for ```+/-```    
- Be sure to reverse the operation when moving things to the other side of the equation   
- Make sure to have a proper scale for graphs   
- Read question carefully and use the appropriate tools to solve    
- **WATCH OUT FOR CARELESS MISTAKES!!!!!!!!!!!**

## Word Problems
- Read carefully    
- model equations correctly   
- ```Reread``` the question over and over again until you fully understand it and made sure there is no tricks. :p      
- ```Lets``` Statement     
- ```Conclusion```

## Graph Problems
- Look up on tips in units (5) and (6)   
- be sure to use a ruler when graphing

## System of Equations
- When in doubt or to check your work, just plug the numbers back in and check if the statement is true

# Credits
- Ryan Mark - He helped provide alot of information for me        
- Ms Hung(Katie) - She helped me check over my study sheet, an amazing teacher!       
- Magicalsoup - ME!
