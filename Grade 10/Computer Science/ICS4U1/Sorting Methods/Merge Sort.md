## Introduction
Merge sort is a comparison type sort that has an effective use of recursion and the divide and conquer algorithm. <br>
Merge sort sorts in $`O(N \log N)`$ time and uses $`O(N \log N)`$ space. <br>
We will explore the pros and cons of this sort the proofs on its time and space complexity and the implementation and algorithm of this sorting method.

**Abstract:** Given two sorted arrays $`a_{lo}`$ to $`a_{mid}`$ and $`a_{mid+1}`$ to $`a_{hi}`$, replace with sorted subarray $`a_{lo}`$ to $`a_{hi}`$.

## Basic Algorithm
- Divide array into 2 halves
- Recursively sort each half
- Merge the two halfs.

```py
def mergesort(left, right, array[]):
    if left <= right:
        mid = (left + right) / 2
        mergesort(left, mid, array[])
        mergesort(mid+1, right, array[])
        mergehalves(left, mid, right, array[])
```

## Running Time Analysis
- Latop computer can run close to $`10^8`$ per second.
- Supercomputer can execute $`10^{12}`$ compares per second.

### Insertion Sort $`(N^2)`$
|computer|thousand $`(10^3)`$|million $`(10^6)`$|billion $`(10^9)`$|
|:-------|:-------|:-------|:-------|
|home|instant|2.8 hours|317 years|
|super|instant|1 second|1 week|

### Merge Sort $`(N \log N)`$

|computer|thousand $`(10^3)`$|million $`(10^6)`$|billion $`(10^9)`$|
|:-------|:-------|:-------|:-------|
|home|instant|1 second|18 min|
|super|instant|instant|instant|


## Proof Sketch

**Proposition** Merge sort uses $`\le N \log N`$ compares to sort an array of length $`N`$.

**Proof Sketch** The number of compares $`C(N)`$ to mergesort an array of length $`N`$ satisfies the recurrence:

```math
C(N) \le C(\lceil N/2 \rceil) + C(\lfloor N/2 \rfloor) + N \text{ for } N \gt 1, \text{ with } C(1) = 0.
```

## Divide and conquer recurrence induction proof

**Proposition**. If $`D(N)`$ satisfies $`D(N)=2D(N/2) + N`$ for $`N \gt 1`$, with $`D(1) = 0,`$ then $`D(N) = N \log N`$.

**Proof** [assuming $`N`$ is a power of $`2`$]
- Base case: $`N = 1`$
- Inductive hypothesis: $`D(N) = N \log N`$.
- Goal: show that $`D(2N) = (2N) = \log (2N)`$

$`D(2N) = 2D(N) + 2N`$ <br>
$`\quad = 2 N \log N + 2N`$ <br>
$`\quad = 2 N (log (2N) - 1) + 2N`$ <br>
$`\quad = 2 N \log (2N)`$


## Resources
- Princeton University: https://algs4.cs.princeton.edu/lectures/22Mergesort.pdf
