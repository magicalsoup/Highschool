# Unit 2: Biology

A person contains roughly 100 trillion cells
- Cells are roughly 20 `μm` (micrometre, 10<sup>-6</sup> m), around 250 cells / cm

## Cell Theory
1. All living things are composed of cells 
2. Cells are the basic units of living organisms
3. All cells came from pre-existing cells

## Eukaryotic versus Prokaryotic Cells
`Prokaryotic cell`: Meaning before/lacking nucleus

`Eukaryotic cell`: Means complete nucleus

|Factors|Prokaryotic|Eukaryotic|
|:-----------|:----------|:---------|
|DNA|In nucleoid region|Usually in membrane-bound nucleus|
|Size|Usually smaller|Usually larger|
|Organelles|Not membrane-bound, smaller|Membrane-bound, more complex|
|Organization|Usually singlecelled|Often form multicellular organisms|
|Metabolism|May not need oxygen|Usually need oxygen|

## Organelles

### Cell Membrane
- Controls what substances **enter/leave** the cell selectively via various receptors/osmosis
    - Allows **nutrients** to enter
    - Allows **waste products** to leave (removal of waste)
- Surrounds and holds other organelles in cell
- Interact with outside chemicals (e.g., hormones)
- Transports food and nutrients into the cell

### Nucleus
- Is the control center of the cell
- Holds deoxyribonucleic acid (DNA) in form of `chromatin`
    - DNA is a double helix containing genes
        - `Genes`: any section of DNA that contains a full set of instructions to make either RNA or a protein, **found** in nucleus
    - `Chromatin`: is DNA **wrapped tightly** in protein
- `Chromosomes`: are even more tightly wrapped `chromatin` used in cell division only, formed when `DNA` **condenses** in `mitosis`
- Surrounded by a double membrane
- Substances enter and exit the nucleus via `nuclear pores`. `Nuclear pores` are holes in the membrane that allow `proteins` and `nucleic acids` into the `cytoplasm`
- Messenger RNA (mRNA) is encoded from DNA and sent to `ribosomes` to produce proteins
- Humans have ~2 meters of genes per cell per nucleus tightly wrapped


### Nucleolus
 - Dense region of DNA located in the nucleus
 - This area of DNA is specially for ribosomal DNA (rDNA), or DNA used to code `ribosomes`, enzymes that assemble proteins
 - Produce "large" and "small" subunits of `ribosomes`, which either form complete `ribosomes` in `cytosol` or mix with `endoplasmic reticulum`, forming `rough endoplasmic reticulum` (RER)

### Cytoplasm & Cytosol
 - `Cytosol` is the fluid cells contain
 - **All organelles** are suspended in `cytosol`
 - `Cytoplasm` is the `cytosol` along with everything in a cell, excluding the nucleus

## Endoplasmic Reticulum
- The endoplasmic reticulum is a network of `tubules` and **flattened sacs** with a <b>*rough*</b> appearance because of the presence of `ribosomes` on the surface
- Network of tubules and flattened sacs
- **Transports** proteins via cytoskeleton in vesicles

## Specific to Rough ER
- Appears "rough" due to the `ribosomes` attached to its outer surface
- Located directly adjacent and attached to nucleus
- Located next to `Golgi apparatus`
- `Ribosomes` in rough ER **synthesize proteins**
    - Transports synthesized proteins to `Golgi apparatus` for packaging and distribution
    - About half the cell's proteins are produced here
- Folds, fixes and **modifies both newly-created and pre-existing proteins** somewhat like **proof-reading**

## Specific to Smooth ER
- **Does not** synthesize proteins
- Appears "smooth" due to lack of `ribosomes`
- Located directly adjacent and attached to nucleus
- Synthesizes lipids (fats, e.g., cholesterol)
- **Metabolises** carbohydrates

## Golgi Apparatus
- Also known as Golgi body, Golgi complex, etc.
- **Receives, modifies and transports** proteins that were produced by the rough ER
- **Packages** proteins into `vesicles` and sends them `cell membrane` for export

## Lysosome
- Spherical vesicle that containing `enzymes`
- **Digests and kills** foreign matter which is then excreted
     - E.g., white blood cells use lysosomes to kill bacteria then spit it out
- **Digests and breaks down** old and unused material/non-functional organelles as needed
- If lysosome ruptures everything dies, hence they are known as "suicude sacs"

## Mitochondria
 - **Singular form is "mitochondrion"**
 - Contains an inner and outer membrane
 - Processes glucose + oxygen gas to form carbon dioxide + adenosine triphosphate (ATP)
    - ATP allows proteins to do things (e.g., spend 1 ATP break 1 molecule)
    - ATP cannot be stored
    - ATP is needed for daily function of the cell

## Cytoskeleton
- Made of **protein filaments**
- **Maintains** and **changes** cell structure, much like a human skeleton + muscular system
    - Moves cells
    - Modifies and adjusts cell structure as needed
- Chemicals can travel along `cytoskeleton`, e.g., `organelles`, `vesicles`, etc.

## Organelles specific to animal cells
1. Centrioles and centrosomes
2. Lysosomes

### Centrosomes
 - Made of same protein as `cytoskeleton`
 - Crucial to mitosis in animal cells
 - **Create and manipulate spindle fibres** during mitosis in animal cells

### Lysosomes
- Explained before.

## Organelles specific to plant cells
1. Cell wall
2. Chloroplast
3. Central Vacuole

### Cell wall
- Provides **structure** and prevents **cell rupture**
- A more stronger, thicker, rigit version of the `cell membrane`
- Made of **cellulose** (type of sugar)
- Also present in most bacteria, fungi, and protists
- The antibotic **penicillin** works by destroying the cell walls of bacteria, killing it

### Chloroplast
- The **solar panel** of the plant cell
- Conducts **photosynthesis**
- All chlorophyll is located in chloroplasts
- Looks green
- Parts of the plant that do not photosynthesize do not have chloroplasts

### Central Vacuole
- Extremely large, may take up to 90% of volume in cell
- Contains water
- Maintains `turgor` pressure against cell wall (pushes against cell wall in all directions)
- Maintains cell shape and resistance
    - Plant cells that lack **turgor pressure** (e.g., celery left in fridge) become **flaccid**

## Cell Division

### Purpose 

### 1. Reproduction
 - Single-cellular organisms reproduce via division **asexually**
 - Multicellular organisms reproduce via combining two germ cells ("sex cells") that contain half the DNA each of two organisms
    - This is **sexual**

### 2. Growth
 - Cells have maximum size before transportation of substances within cell becomes **inefficient**, due to **larger cells** decreasing efficiency of `diffusion`
 - Cells transport chemicals (e.g., nutrients) via `diffusion`, this **limits cell size**
 - The only way to maintain proper function and get bigger is to **add more cells**


### 3. Repair
- **Organisms need to repair cells to stay alive and maintain proper health**
- Millions of cells are replaced everyday
- Cells naturally die and need to be replaced
    - e.g., red blood cells, hair cells, skin, injuries, broken bones

### Cell cycle

<img src="https://www2.le.ac.uk/projects/vgec/diagrams/22-Cell-cycle.gif" width="600">

- **Interphase**
    - Large majority of a cell's time is spent in interphase
    - **G1**: (normal growth and function), 
    - Prepare for cell divison
    - **S**: Replication of DNA
    - **G2**: Replication of organelles
    - Checkpoints
        - Cells check various things before progressing through various stages in interphase
        - Causes of stopping via checkpoints include damaged DNA, not replicated DNA, lack of nutrients for cell growth, and/or signals from other cells
- **Mitosis**
    - Occurs only in eukaryotic cells
    - P-MAT: Prophase, Metaphase, Anaphase, Telophase
    - Division of the nucleus
- **Cytokinesis**
    - **cell division**
    - The parent cell splits into two daughter cells
- **G0**
    - Cell no longer divides ("cell cycle arrest")
    - Outside of cell cycle 

### Mitosis
- `Chromatid`: Supercoiled DNA, only visible during mitosis, cannot be read without unwinding, similar to compressed zip file
- `Chromosome`: Two identical "sister chromatids" held together in centre by `centromere`, or one sister chromatid after anaphase
- `Centromere`: Proteins sticking sister chromatids
- PMAT (prophase, metaphase, anaphase, telophase)
- Division of the nucleus

| Phase | Diagram | Description |
| :--- | :--- | :--- |
| Prophase | <img src="http://www.edupic.net/Images/Mitosis/prophase_3D.png" width="250"> | - Chromatin condenses into two identical `sister chromatids` which condense into `chromosomes` <br>      - Happens to 23 pairs of chromosomes <br> - Nuclear membrane dissolves <br> - Centrosomes move to opposite ends (`poles`) of cell, creating `spindle fibres` that begin to attach to `centromeres` in animal cells |
| Metaphase | <img src="http://www.edupic.net/Images/Mitosis/metaphase_3D.png" width="250"> | - Chromosomes line up in centre of cell to ensure they divide evenly <br> - Everything in prophase has completed (e.g., nuclear membrane has dissolved completely) |
| Anaphase | <img src="http://www.edupic.net/Images/Mitosis/anaphase_3D.png" width="250"> | - Centromeres split, separating sister chromatids <br> - Sister chromatids are pulled towards opposite sides of cell via shortening spindle fibres <br>     - Sister chromatids are now called `daughter chromomsomes` |
| Telophase | <img src="http://www.edupic.net/Images/Mitosis/telophase_3D.png" width="250"> | - Effectively opposite of prophase <br> - Nuclear membranes form across each of the two new nuclei <br> - Daughter chromosomes unwind into chromatin and are no longer visible <br> - Nucleolus forms in each nucleus <br> - Spindle fibres break apart <br> - **Cytokinesis** usually begins in telophase <br>     - Cells starts to **cleave** (cell centre starts to pinch itself) |

### Cytokinesis
 - Cell division
 - Cell splits completely to two daughter cells
 - In **animal cells**: Cell membrane pulled inward by cytoskeleton
 - **"Pinches in"** along equator of cell, forming **"cleavage furrow"**
 - In **plant cells**: Golgi apparatus produces and sends vesicles to centre of plant cell **"cell plate"** to make new cell wall and membrane between daughter cells

## Cell Specialization
- `Zygote`: A single-celled organism formed from the fertilization of an egg by a sperm cell, is a totipotent stem cell
- A cell's position in the `gastrula` (outer, middle, inner layer) will determine the fate of the cell, or its potiental. 
- Chemical signals from other cells will also determine activated genes that lead to specialisation
- (LOCATION LOCATION LOCATION!)
- Specialisation is determined by reading only certain genes

### Stem Cells
- **Unspecialized** cells with the potential to become one of several types of cells. 
- Can either divide to two stem cells or one stem cell and one specialised cell
- Specialised cells generally do not divide

| Type of Stem Cell | Obtaining | Potential | Pros | Cons |
| :--- | :--- | :--- |:--- | :--- |
| Totipotent | Morula (16-cell ball) 3-4 days after lab-fertilised zygote | Unlimited | Unlimited potential, does not initiate immune response | Ethical concerns of destroying fertilized embryos |
| Pluripotent | Blastocyst (200-300 cell ball) 4-7 days after fertilisation | Nearly unlimited | Nearly unlimited potential, no need to create new embryo as most are taken from discarded in vitro fertilisation | Ethical concerns of destroying embryos, greater chance of initiating immune response |
| Multipotent | Adult stem cells | Limited to cells of their group/organ/location (e.g., blood stem cells to red blood cells, white blood cells, etc.) | Easy to harvest, easy to find | Immune response, limited potential |
| Induced pluripotent | Multipotent stem cells | Reprogramming multipotent stem cells using embryonic genes using a virus | Same as pluripotent | Does not require new embryos, immune response not expected, high potential | Technology not there yet to make this possible |

### Potential uses of stem cells
 - Studying cell growth and function
 - Testing drugs on specific target cells
 - Lab-grown meat for vegetarian purposes
 - Regenerative medicine to replace tissues (e.g., blindness, bone marrow transplant, cancers, limb regrowth)

## Telomeres

## Cancer
 - Group of diseases that involve out-of-control cell division which may spread throughout the body
 - `Tumour`: Uncontrolled lump of cells that do not perform normal cellular functions
 - `Benign`: Describing a tumour that does not metastasise or interfere with normal cell function (harmless, non-cancerous)
 - `Malignant`: Describing a tumour that does interfere with normal cell activity and metastasise
 - `Carcinoma`: Cancerous/cancer
 - `Metastasis`: Primary (original) tumour spreading throughout the body to create secondary tumours
 - `Carcinogens`: Anything that can cause cancer, e.g., chemicals, radiation/energy, some viruses
 - `Neoplasm`: A solid or fluid-filled sac that is formed by uncontrolled cell growth (e.g., tumours)
 - Random mutations can also lead to a cancer cell due to irregular DNA replication
 - Generally, multiple mutations in several key genes are required for a cell to become cancerous
 - Cancer is *not* contagious, neither can it be inherited
    - A genetic predisposition to cancer *can* be inherited
 - By the time cancer is detected, it can contain millions of cells that have been growing for years

### Cancer screening
 - PAP smear for cervical cancer
 - Mammogram for breast cancer
 - Colonoscopy for colorectal cancer
 - PSA blood test for prostate cancer

### Cancer diagnosis
 - Endoscopy (using a flexible camera with tissue extractor to search for cancers of the respiratory and/or digestive systems)
 - X-rays
 - Ultrasounds for soft tissues
 - CAT/CT scan (more x-rays)
 - MRI scan (uses radio waves and magnetic fields)

### Cancer treatments
 - Surgery
    - Physically removing tumour with stabby things
    - Ineffective if cancer has metastasised
    - If even one cell escapes the stabby cancer can regrow
 - Radiation therapy
    - Blasting radiation at tumours so that their DNA becomes so damaged that DNA replication, and, as a result, cell division is impossible
    - Can harm neighbouring cells
    - Ineffective if cancer has metastasised
 - Chemotherapy
    - Blasting drugs that kill dividing cells
    - Does not feel very good for the patient
    - Fast-growing cells may die off (e.g., hair, skin cells)
 - Biophonics
    - Using light beams to detect and treat cancer

## Organ systems
**Business model for organ/organ systems**

| Business thing | Corresponding organ/organ system |
| :--- | :--- |
| Management | Central nervous system (brain) |
| Messaging | Endocrine + peripheral nervous systems |
| Workplace | Body | 
| Transport | Circulatory, digestive, urinary systems (internal, import, export, respectively) |
| Storage | Fats |
| Cash flow | Digestive + respiratory systems |
| Security | Immune + integumentary (skin) systems |
| Workers | Cells + muscular system |

## Tissues
 - `Tissues`: Different cell types grouped together performing the same task
 - Organisms have a hierarchical organisation
 - Basic tissues: Connective, muscle, nervous, and epithelial tissues
 - Epithelial tissue
    - Tightly packed cells that line body surfaces, e.g., skin
 - Connective tissue
    - Produces collagen fibres that support organ structures and bone, e.g., ligaments (bone -> bone), tendons (muscle -> bone)
 - Muscle tissue
    - Fibrous tissue that can be subdivided into cardiac (heart), smooth (digestive), and skeletal (voluntary) muscle tissues
    - They contract
 - Nervous tissue
    - Responds to external/internal stimuli, e.g., brain, nerves

## Digestive system
 - Two types of digestive systems
 - Bag digestive system
    - One way in, same way out (e.g., coral, jellyfish)
 - Tube digestive system
    - One way in, another way out (e.g., worms, humans)
    - Mouth -> esophagus -> stomach -> small intestine -> large intestine -> rectum -> anus all part of the tube
    - Gallbladder, liver, salivary glands, and pancreas produce digestive enzymes/juices in humans
    - Process of eating food: **Ingestion** (eat) -> **digestion** (physical and chemical breakdown) -> **absorption** (of nutrients to bloodstream) -> **egestion** (poo)
    - Flies digest before ingesting
    - `Jujunum`: Centre of small intestine
    - `Duodenum`: Beginning of small intestine
    - `Ileum`: End of small intestine
    - `Rectum`: Holds waste to be excreted voluntarily
    - `Anus`: Controls waste to be defecated voluntarily
    - `Appendix`: Used to be used to digest plant matter, now virtually useless in humans
    - `Gallbladder`: Stores and secretes bile as buffer between liver and small intestine that helps break down fats (lipids)
    - `Ruminants`: Herbivores that digest food using a chambered tube
        - Chew -> Reticulum and rumen (first and second stomachs) -> regurgitate and rechew -> Omasum (third stomach) -> Abomasum (fourth stomach) -> small intestine -> large intestine -> waste
    - `Eoprophagy`: Consumption of feces

### Human digestive system
 - Mouth ingests food
 - Teeth, tongue, and salivary glands work to begin digestion
 - Esophagus squeezes food down in waves (peristalsis) down its smooth muscle tube
 - Stomach
    - Mixes hydrochloric acid with digestive enzymes to break down food
        - Hydrochloric acid is diluted and does not break down the food itself much, enzymes are more effective at a lower pH
    - Liquifies food and kills bacteria
    - **Goblet cells** produce **mucous**, which lubricates the stomach and intestines, protecting the stomach
    - Made of smooth muscle to churn food, somewhat like cooking with enzymes or a washing machine
 - Intestines
    - Pancreas makes most digestive enzymes and pumps them in the duodenum
    - Absorbs nutrients and water to bloodstream
    - Forms and excretes feces
    - Contains smooth muscle to continue peristalsis
    - Contains plenty of blood vessels for faster nutrient absorption
    - Intestinal epithelium
        - Optimised for surface area
        - Folds contain `villi` (singular, "villus")
        - Villi contain capillaries and absorbing and goblet cells
        - Absorbing cells caintain microvilli, which absorb nutrients via diffusion

## Respiratory system
 - Exchanges oxygen gas and carbon dioxide gas between red blood cells and the surrounding air, which is required for cellular respiration
 - Diaphragm contracts to lower itself, causing the rib cage to rise, which increases lung volume, which subsequently causes pressure to decrease and air to rush in to the lungs
    - Diaphragm relaxes to return everything to its normal position
 - Air is warmed and moisted while passing through nasal cavity blood vessels
 - Trachea and bronchi are made of rigid cartilage rings
    - Prevents airways from closing, similar to a vacuum hose
 - Respiratory epithelium
    - Contains goblet and ciliated cells
    - `Cilia`: Singular "cilius", sweep mucous out of the lungs and throat
    - Nose hairs and mucous trap debris which is swept out by cilia
 - Alveoli (singular "alveolus") epithelial tissue is one cell thick
    - Surrounded with capillaries which exchange gases via diffusion
 - Trachea -> 2 bronchi -> bronchioles -> alveoli
 - Gas exchange
    - Swapping of carbon dioxide and oxygen gas between the bloodstream and the environment (e.g., red blood cells and alveoli)
    - A large surface area, thin membrane, and moisture are all required for optimal gas exchange
    - Alternate gas exchange systems include
        - Fish using a constant water flow forcing dissolved oxygen through their gills
        - Gills stick together out of water, resulting in suffocation
        - Frogs use lungs on land, but can also perform gas exchange underwater using their skin

## Circulatory system

<img src="https://i.pinimg.com/originals/06/0d/48/060d48aa995e2da30405e7cef07679f8.png" width="400">

 - Interacts with literally every other system
 - Carries oxygen and nutrients to cells, carries carbon dioxide and waste away

### Components
 - Composed of heart, arteries, veins, and capillaries
 - `Arteries` flow **away** from the heart
    - Made of **thick** muscle layers and elastic connective tissue
    - Muscle layers must withstand and **maintain** higher blood pressure throughout body due to proximity to heart
    - May `vasoconstrict` or `vasodilate` to increase or restrict blood flow, for example, to blush or to pale, respectively
 - `Veins` flow **to** the heart
    - Made of **thin** muscle layers and elastic connective tissue
    - Carries low pressure blood with valves to ensure one-way flow
    - Blood moves by movement of skeletal muscles pushing blood
 - `Capillaries` are one cell thick
    - They transition between arteries and veins
    - Blood cells are forced to go in single file
    - Present, amongst other places, in alveoli and villi
 - The `heart` pumps blood throughout the body
    - Has one-way valves
    - Has four chambers, two `atria` (sing. `atrium`) and two `ventricles`
        - Blood is returned to atria which push them to ventricles which push them out of the heart
    - **Right** side of heart receives and sends **deoxygenated** blood **to** lungs
    - **Left** side of heart receives and sends **oxygenated** blood **from** lungs
 - Invertebrate circulatory systems are either **open** or **closed** (douse everything with blood then collect or use vessels like we do, respectively)
 - Most invertebrates have an open circulatory system

### Myocardial infarction
 - Also known as **heart attack**
 - When `atherosclerosis` occurs in `coronary arteries` (when fatty plaque deposits build up in arteries feeding the heart)
 - If clots break open a larger clot forms over it
 - This repeats until the artery is completely blocked, leading to death of cardiac muscle cells
 - Caused by lifestyle choices, although predisposition can be increased due to genes

### Blood
 - Composed of red blood cells, white blood cells, platelets, and plasma
 - `Red blood cells`: Biconcave discs carrying oxygen and carbon dioxide to and from cells, respectively, using `hemoglobin`
    - Denucleated, instead packing as much hemoglobin as possible inside
    - Hemoglobin and oxygen give them their colour
 - `White blood cells`: Part of the immune system, they neutralise and remove foreign threats
    - Can make antibodies
    - Can engulf and kill pathogens
    - 700:1 ratio of red blood cells to white blood cells
 - `Plasma`: Clear fluid made of 90% water filled with proteins and dissolved nutrients
 - `Platelets`: Irregular colourless "bodies" that form **fibrous** clots

## Immune system
 - `Pathogens`: any**thing** that cause disease
    - Pathogen waste can be toxic which cause symptoms of disease
 - Passive defense
    - Skin - physical barrier
    - Sweat/tears - `lysozymes` kill bacteria
    - Stomach acid - it's acid dangit acid kills things
    - Beneficial bacteria overpopulate surfaces to prevent harmful bacteria from settling
 - Adaptive defense
    - `White blood cells`: For the sake of G10, divided into two subtypes:
        - Cells that engulf and consume bodies (`phagocytes`, e.g., macrophages)
        - Cells that produce antibodies (`plasma B cells`, i.e., plasma B cells)
    - `Antibodies`: Secreted proteins that stick to a specific molecule found on pathogens
        - Clumps pathogens together for simple cleanup and prevents them from spreading
        - Covers and prevents toxins from reacting
        - Acts as a flag for phagocytes to destroy marked pathogen or toxin
 - Acquired immunity
    - After initial immune response, antibodies are still produced for that type of pathogen
    - Once pathogen is detected again, "memory cells" reactivate and kill things faster
    - Much faster than initial response, typically resulting in no symptoms
    - This is why you generally can never be sick from the same pathogen twice
    - Pathogens mutate (e.g., influenza) so that they are no longer recognisable by antibodies
 - `Vaccination`: Injecting a small amount of a **dead/weakened** version of pathogen giving acquired immunity without actually getting disease
    - There may be mild side effects
    - `Boosters` are required for some vaccines as "memory" fades over time (e.g., tetanus)
    - `Herd immunity`: When enough of the population (90% in general) is immune to a disease, drastically reducing rate of disease even amongst those not immune
        - Those who cannot be vaccinated for whatever reason are protected due to a far lower chance of encountering the disease itself
    - Prevention (vaccine) > cure (treatment)
    - Chance of disease from the vaccine are far lower than chance of death or serious infection from a pathogen
    - Vaccines do not cause autism or seizures, but may act as a trigger for the latter due to genetics

## Musculoskeletal system
 - Maintains **structure**
 - **Protects** other systems and cells
 - Enables **movement**
 - Four types of **connective tissues**: Ligaments, tendons, bones, and cartilage

| **Bone** | **Information** | **Location** |
| :--- | :--- | :--- |
| Clavicle | Collarbone | Collar |
| Humerus | Funny bone | Lower upper arm |
| Femur | Largest bone | Thigh |
| Tibia | One of two bones in lower leg | Front lower leg |
| Vertebrae | Enable spinal movement | Spine |
| Patella | Prevents leg overextension and protects knee join | Kneecap |

### Bones
 - Hard and dense
 - Bone cells produce minerals (e.g., phosphorus + calcuium) and **collagen**
    - Minerals for strength, collagen for flexibility
 - Bone marrow produces blood cells
 - Contain blood cells

### Joints
 - Anywhere where two bones meet
 - Types of joints
    - Hinge joint (e.g., knee, elbow)
    - Ball and socket joint (e.g., Hip, shoulder)
    - Fixed (e.g., skull, pelvis)
 - `Ligaments` connect bones across joints
 - `Cartilage` cushions bone on each side of joint and allows for smooth motion

### Skeletal Muscle
 - Made of **striated** muscle fibres of long cells
 - Voluntary muscles, receive signals from brain via nerves
 - Always come in pairs as **any muscle can only pull, not push**
 - Must be attached to two bones in order to move a bone

## Nervous System
 - Coordinates body activities
 - `Central nervous system`: Brain + spinal cord
 - `Peripheral nervous system`: All other nerves connecting everything to spinal cord/brain
 - `Neurons` send electric signals down their singular long `axon` branch thing
    - They accept signals from `dendrites` on the main cell body
    - `Schwann cells` form the `myelin sheath` to insulate and nurture the axon, protecting it from interference
    - Neurotransmitters are chemical signals that transmit information between neurons
    - Electrical signals tell chemical signals to go to other neurons
 - Nerves
    - Bundle of axons
    - Surrounded with blood vessels and connective tissue
    - Nerve signals are short-lived, fast, and targeted towards specific groups of cells

### Disorders
 - Parkinson's disease
    - Loss of brain neurons that send neurotransmitters to muscles
    - Leads to muscular and mental decline
 - Multiple sclerosis
    - Immune system attacks myelin sheath
    - Disruption of neurons' electric signals due to lack of protection
    - Causes spasms and loss of muscular control
 - Alzheimer's disease
    - Protein deposits (plaque) build up in brain tissue
    - Leads to memory loss and total system failure

## Endocrine System
 - Coordinates organ functions
 - `Hormone` chemical signals produced by endocrine glands that, compared to nerve signals, are **long-lasting**, **slower**, and **general**
 - Specific hormones bind to specific receptors on specific cell membranes (e.g., mailing lists)
 - Hormones travel through the bloodstream
 - Hormones either encourage or discourage activity
 - Can cause positive or negative feedback loops with glands

| **Endocrine organ** | **Purpose** | **Location** |
| :--- | :--- | :--- |
| Pituitary gland | Controls growth and development | Brain |
| Pancreas | Secretes insulin to ensure sugar in bloodstream is taken in by cells | Attached to duodenum |
| Gonads (ovaries + testes) | Secretes the reproductive hormones testosterone and estrogen, respectively | Lower abdomen |
| Adrenal glands | Control stress response, secrete adrenaline (fight/flight response) | Above kidneys |

### Disorders
 - Type 1 diabetes: The pancreas is unable to produce any insulin, resulting in high blood sugar
     - Generally caused by genetics
 - Type 2 diabetes: The pancreas produces not enough insulin and/or cells are resistant to it, resulting in high blood sugar
    - Generally caused by lifestyle choices (e.g., diet)
 - Growth disorders (dwarfism/gigantism)
    - Caused by poor pituitary and/or hypothalamus function or endocrine gland damage as an adult