### 1. What is measles
measles is a highly contagious diease caused by a virus, it is airborne, and famous symptoms include the rash/spots/dots appearing on body. 

Is the leading cause of death of young children and is a diease that is preventable with vaccines.

### 2. Compare the global death rate in 1980 vs 2015

|  |1980|2015|
|:-|:---|:---|
|Rate Of Deaths Per Year|$`2.6`$ million deaths|$`134200`$ deaths|
|Rate Of Death Per Day|$`7200`$ deaths|$`367`$ deaths|
|Rate Of Death Per Hour|$`120`$ deaths|$`15`$ deaths|

### 3. What is the main reason for this difference

The measles vaccine, and the amount of children that got the vaccine

### 4. Does everyone who gets the vaccine become immune to the disease they were vaccinated for? What is the percent success rate for the measles vaccine?
Not 100% but most/high percentage of people who get vaccinated will become immune to the disease they were vaccinated for.

The scucess rate for the measles vaccines was $`85\% - 95\%`$ when delivered between childrens 12-15 months of age, and $`97\%`$ with a second dose at a later age.

### 5. Give two reasons why Disneyland in California is a more likely area for a measles outbreak to occur.

Disneyland is populated with people and people are from around the world, which increases infection rates due to the close proximity the people are to each other and the chances
of people from different parts of the world to bring the disease to it, such as the case in california. Furthermore, more people in california are not getting the MMR shots (measles-mups-rubella),
which increases the risk of infections and renders herd immunity useless. Lastly, disneyland contains many young children, which are more susceptible to the measles virus, and even some who cannot get or have gotten their shot yet.

### 6. How many people were affected by the measles outbreak at Disneyland, and how old were they?
The infected range from 7 months to 70 years old

at least 70 cases were reported in 6 states of mexico, according to Google, 147 cases were from Disneyland

### 7. Why was Jennifer Simon upset about the anti-vaccination parents who decided not to vaccinate their children? i.e. Who can be impacted by parent’s decision not to vaccinate their child?

She was upset because those people affect her children, since her child was under risk of getting the measles virus from the anit-vaccination parents and their children.

A parents decison not to vaccinate their children can render herd immunity useless, and put other children and people under risk of contracting the disease.

### 8. What is "herd immunity" and how does it work?
When a large percetnage of people are vaccinated against a certain disease, which makes it very hard for the disease to spread and helps protect those who cannot be vacacinated against the disease (age, condtions, etc).

It works when a large percentage of people are vaccinated.

### 9. What percentage of the population needs to be vaccinated in order to achieve herd immunity?

Usually $`90\%`$ is needed inorder for herd immunity to work. For very contagious diseases, such as measles, $`95\%`$ of people need to be vaccinated inorder for herd immunity to take place.

### 10. Barbara Loe Fisher heads a group that supports the right to opt out of having their kids vaccinated. She was quoted in the Disney outbreak article that it is not wise or responsible to blame unvaccinated people for the Disney outbreak because a small number of those stricken had been fully vaccinated. Use scientific knowledge from to respond to this statement.
This statement is false. Since only a small number was vaccinated, herd immunity was useless, which could have infected many unprotected children. Furthermore, without getting vaccinated, you have a high chance of getting the disease if being in contact with those infected around you. 

**Ask rozen about this question**

### 11. Which two socio-economic groups are least likely to get their kids vaccinated?
The low and high socio-economic groups.

### 12. Explain the most likely reasoning that each of these socio-economic groups are using to choose not to have their kids vaccinated.

Low socio-economic: vaccines can be difficult to access, even if they are free

High socio-economic: mistrust of medical interventions, debunked notions of a link to autism or seizeus, or other factors.

