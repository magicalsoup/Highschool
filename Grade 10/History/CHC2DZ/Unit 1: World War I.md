// TODO: rename title to be more general
// TODO: fix rest of sheet since this is a data dump
// TODO: Add information before and after
// TODO: Include OPCVL

# World War I

## MAIN long-term causes of WWI

- `Militarism`: Military competition between countries causes arms races
- `Alliances`: Countries become larger forces by joining forces with others to from larger "teams"
- `Imperialism`: Eating countries causes even larger forces and conflicts
- `Nationalism`: Patriotism and pride (sore losers)

## Timeline

 - European society becomes increasingly **militaristic** and **nationalistic**
    - People **welcomed** war
 - Austria-Hungary implicates Serbia in assassination of Franz Ferdinand
 - German government promises to support Austria-Hungary
    - Similar to `blank cheque`: Promise to do anything
 - Austria-Hungary sends Serbia an ultimatum
    - Stop all hatred against Austria-Hungary (sure)
    - Punish those involved in assassination (sure)
    - Let Austro-Hungarian police yeet terrorist organisation responsible for assassination (heck no)
 - Serbia refuses to allow the last request, causing Austria-Hungary to declare war
 - Russia tries to come to Serbia's aid because they are friends
 - Germany declares war on Russia and France
 - Germany tries and fails to enact Schlieffen Plan
    - As they invade Belgium, Britain declares war on Germany over "a scrap of paper" (75-year-old treaty with Belgium to defend it)

## Schlieffen Plan

 - German plan to avoid a **two-front** war with Russia and France
 
1. Run through Belgium really quickly
2. Kill France
3. Rush back to Germany
4. Kill Russia
5. ???
6. Profit

 - Failed due to following German assumptions:
    - Russia would take longer than six weeks to mobilise forces
    - France would die in six weeks
    - Belgium would not resist
    - Great Britain would not come to Belgium's aid and join the war

## Canadian Expeditionary Force

 - Not all recruits welcomed
 - In 1914, the following were required: Good eyesight, arched feet, healthy teeth
 - Most visible minorities were rejected because recruiters were racist
    - First Nations were valued later in the war for sniper and recon abilities
 - Samuel Hughes
    - Minister of Militia and Defense
    - Appointed by PM Robert Borden to recruit and train Canadian Expeditionary Force
    - Army grew from 3 000 to 30 000 within two months
    - Established Valcartier Camp
    - Helped industrialise Ontario
    - Bought expensive **shovel-shields** (shovels with holes)
    - Issued flawed and useless garbage **Ross Rifles**
    - Lots of corruption accusations with substandard munitions production
    - Religious bigotry (intolerance) of French Canadians
    - Resigned in 1916
 - Valcartier Camp
    - Largest military camp on Canadian soil
    - Established in Quebec City by Hughes
 - British perspective
    - Canadians were wild, mutinous, and unruly
    - Crashed officers' private bars
    - Ignored British class structure rules
    - 1 249 cases of STIs out of force of 30 000
 - Less volunteers as war continues because idealist war fantasies are fading
    - Government relaxes restrictions (i.e., lowered medical standards/requirements)
 - First Nations
    - Wanted to fight for British Crown
    - Wanted to gain rights
    - 1/3 of all First Nations enlisted
    - Typically **sniper** or **recon** as they had **traditional hunting** skills
    - Cultural differences between Canadians and First Nations were challenges (e.g., challenging the leader)

## Trench warfare

 - British + French vs Germans
 - `Western Front`: 700 km of trenches from Belgium to Switzerland
 - War of attrition
 - It sucked
 - Government spams propaganda
 - Modern society thinks that this was not very intelligent
 - Hygiene is all gone
    - Mud, lice, and rats everywhere
 - `Duck boards`: Wooden boards used to cross wet, marshy land
    - Used to prevent soldiers' feet from rotting

 1. Everyone go just go please don't die but charge them
 2. If you fail and we run out of people to throw run away
 3. Everyone who charges dies
 
## Technologies

 - Machine guns
    - Better used defensively than offensively due to bulk
 - Artillery
    - Field guns/cannons
    - `Big Bertha`: Powerful German artillery cannon
 - Airplanes
    - First used for recon
    - Pilots smuggled guns onboard
    - Germans developed integrated gun that synched with propeller to shoot through it
    - `Dogfight`: Aerial combat
 - Tanks
    - Invented in 1916 to combat machine gun
    - Too slow and would get stuck
 - U-Boat
    - Submarines used to disrupt supply lines
    - Used to "starve another country into submission"

### Chemical warfare

 - Chlorine gas
    - Reacts with lining of respiratory system
    - Causes lungs to fill with bodily fluids
    - Drown
 - Mustard gas
    - Causes internal + external bleeding
    - Odourless
    - Active for weeks
    - Causes painful blistering