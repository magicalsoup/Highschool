# Unit 1 Review
- I know, we have had alot of review sheets already, but I hope this one will be more complete than the others.
- This one follows the review doc by Ms weatherall

## Causes of WW1

There are 4 things (the MAIN) causes you need to understand why/how WW1 happened

First is `militarism`: the so called arms race. In essence, two nations compete with each other about military technology, who has the best weapons, quantity, etc.
- eg. US makes atomic bomb, Russia does the same
- Basically who has the better army
- This can lead to war, or at least put tension on each other because if someone wins, they can easily oblierate the other in a future war.
- This race, as explained above, is driven by the `fear` of the other country becoming dominant and having military superiority.

Second is `Alliance`. The mentality is that well, if we are equal in terms of individual strength, then I guess the side with my support wins. Even if countries of
the same military strength, a side with more countries backing it up, will definetly have a huge advantage. (Strength in numbers). Or maybe the country has weak military power, but has someone with strong military power backign them up. (Similar to how the Austrians got backed up by the Germans).

There were some Alliances made during/before WW1:

`Triple Entente`: Consists of Great Britain, France, and Russia.

`Triple Alliance`: Germany, Italy, and Austria-Hungary.

Similar to the arms race, once a country has a powerful enough force (either from arms or alliance), they can declare war on weaker countries and take them over, similar to the next point, imperalism.


Imperialism is pretty simple. A country expands and takes over other countries to become more powerful. 
- War can happen when countries try to prevent this from happening, or when competeting for territory if they also want to take over.

Nationalism is pretty simple. Its:

1. The feeling of dedication and love for your own country.
2. The desire to be free of foreign control.

This first point can lead to war as someone overly thinks their countries is the greatest and should rule the world (cough cough Germany). 
The second point is when countries don't want to be controlled by another foreign force, and wish to fight back. (cough cough again Germany, and this time Russia as well).

## Assaination Of The Duke
First some key terms for context:

`Bosnia`: a province in the Austrian Hungarian Empire with large population of Serbians. `Sarajevo` is its capital city.

`Serbia`: an independentc country on the border of Austria Hungary.

`Black hand`: Serbian terroist group comitted to independence of all Serbians.

Picture this, you are a student, who just recently joined a terroist group, and now kills people, I mean, is there ever a possiblity that exists such a student. Oh wait, its
`Gavrilo Princip`. You see, this man was part of the `Black hand`, and was the assain of the duke. And for the `Black Hand`, their head was a Serbian millitary inteligence cheif, and obviously had no trouble smuggling in weapons to train the terroist group.

Now you have a group that wants to unify Serbia, now which is the most likely target perhaps someone who has country over the country? Oh yes, right, the Duke.
Story goes on how the assaination actually failed (Gavrilo threw the grenade, got `no u'd` by the duke), but the driver took the same route and the duke was assainted again (this time with a gun, so hes definetly dead this time.)

Now now, so the duke died, its only one person, so no war right? WRONG! (Bruh, are you ok). However, being the ~~lazy~~ officials they were, the Austro Hungarian officials alot of time to implicate Serbia for the assaination, and a WHOLE FRICKING MONTH to go to war.

So why did they wait so long? Well, Serbia is protected by their good pal Russia, so the Austria needed a power that would rival them, (Germany). Germany gave the Austrians a blank cheque (basically saying, do whatever you want, I will always support you in whatever you do).
And boom the Austria Emperor (Franz Joesph) thought if Germans helped them, Russia would not dare get involved.

So being the cocky people they are, Austria sent an ultimatum to Serbia
1. Stop all hatred against Austria Hungary (sure)
2. Punish all those involved in the assaination (yeah, seems fair)
3. Allow Austrian Hungarian police to come into Serbia & destroy the Black Hand organization (Oh hell no)

You see, the third request cannot be fufilled. If it was, then technically Serbia is allowing Austrian Hugarain to march their troops in freely, and theres no way a country would allow that.

And so on **July 26, 1914**, Austria Hungary declared **war** on Serbia.

## The Start Of War

Of course, being the mad lads they were, people were **primed** for war. This was due to millitarism and nationalism. Each side prepared for war a long time before this, and had plans for situations already prepared.

And of course, Russia not being a bussy, mobilizes its army to protect its Slavic brothers (Serbia).

And of course, Germany declares war on Russia due to the cheque. The also declare war on France.

To invade France, they would want to get a better ground (the high ground), so they invade neutral Belgium in the effort to attack France. They thought that Great Britain wouldn't interfere, but Belgium pulls the (You activiated my trap card) move, and Great Britain declares War on Germany to honour a 75 year old treaty with Belgium.

And of course, ~~since this is Canadian history~~ since Canada was part of the British, Canada was forced to go into the war.

## The Schlieffen Plan

Well, Germany is fighting a war on two fronts! (Geographically). This is not good, so they tried to employ the schlieffen plan.

So lets play, assumptions and reality:

|Assumptions|Reality|
|:----------|:------|
|Russia would take more than 6 weeks to mobilize|Russia mobilized in 10 days.|
|France would be defeated in 6 weeks|The war lasts 4 years|
|Belguim would not resist|Belgian and British support held back the Germans|
|Britain would not get involved|Britain came to the aid of Belgium right away|

If you guessed all of the assumptions were wrong, you are correct! So The schlieffen plan totally failed. (Germany got an F in War tatics).

So why didnt it work? Because assumptions = make an asse of u and me, so of course it didnt work. Okay, seriously, it was due to the reality above.

## Canada's Entry into WW1

Some key figures

`Robert Borden:` Conservative Prime Minister of Canada during WW1
- poor public speaker
- very loyal to his friends
- very serious
- nationalistic

`Valcartier Camp:` Training camp for Canadian soldiers
- built really fast
- alot of things were installed
- largest military camp ever seen on Canadian soil

`Sam Hughes`: Minister of Militia and Defense in Borden's cabinet, also a clown.

**The War Measures Act 1914**: Gave the government sweeping powers
- Censorship
- Price and Wage controls
- Pass laws without parliamentary approval
- Arrests, detentions without trial, deportations
- Basically the essential item for a ~~dicator~~ prime minister

So of course, comes with war is **Wartiem Propanganda**
- Media encourged men to serving their country, which meant fufilling their duty to King and Country.
- recruitment posters urged enlistment on basis of patriotism and emotional connectiosn to the war's major issues
- Later, desparate posters shamed men into enlisting
- Wartime propanganda urged women to pressure men to enlist

Some Beliefs in Canada during that time:

**Social Darwinism**: Provided "scientific" explanation and social justification for racial inequality, cultural exploitation and laissez-faire capitalist activity

**Imperialism**: belief in progress and in white superiority was taken for granted throughout the Western world. Many English-speaking Canadians believed that the British principles of government, were the best and that Canada's greatness depended on its British heritage

Sadly there was still some questionable/racist decisions. Requirements were strict:
- 18 - 45 years old
- good eyesight arched feet, healthy teeth(???)
- 5 feet 3 incches tall at least (yay I'm taller than that)
- The guy behind the desk decided everything, this caused the racism. Most non-white recruits were rejected.
- Later on, the requiements loosened as less people wanted to go to war.

## Sam Hughes
- Appointed by `Robert Borden` to recurist and train new troops
- Turned 3000 troops to 30,000 within 2 months

### ShovelShield 
wasted $34,000 to make 25,000 sheleves with a hole in them. Really inefficient (could not dig). Bad idea.

### Ross Rifle
Canadian made rifle, but got jammed easily from dirt in the trenches, bad in trenches, bad idea in general.

The saying goes that Canadian would pick up rifles from dead British troops rather than using the ross riffles.

### Corruption
He appointed close family members and friends as high positions within the military and government.


Due to these reasons, he got fired (duh). His only success was Val Cartier Camp and industrialize Ontario.

## Canadian Troops
- Considered wild, unruly, and mutinous. Refused to salute officiers. Crashed at private bars for officiers. 
- They fricking burned down a tent caused they were showed the same movie 3 times
- They be doing it, 5% of the 30,000 had STD's

## Non-Canadian Recruits
