## Unit 3

## Review

Some special ratios you should know:

$`\csc \theta = \dfrac{1}{sin \theta}`$

$`\sec \theta = \dfrac{1}{\cos \theta}`$

$`\cot \theta = \dfrac{1}{\tan \theta}`$

## Special Angles

These are the angles in trigonometery that have "nice" solutions, and does not require a calculator.

|Degrees|$`0^o`$|$`30^o`$|$`45^o`$|$`60^o`$|$`90^o`$|
|:------|:------|:-------|:-------|:-------|:-------|
|$`\sin \theta`$|$`0`$|$`\dfrac{1}{2}`$|$`\dfrac{\sqrt{2}}{2}`$|$`\dfrac{\sqrt{3}}{2}`$|$`1`$|
|$`\cos \theta`$|$`1`$|$`\dfrac{\sqrt{3}}{2}`$|$`\dfrac{\sqrt{2}}{2}`$|$`\dfrac{1}{2}`$|$`0`$|
|$`\tan \theta`$|$`0`$|$`\dfrac{1}{sqrt{3}}`$|$`1`$|$`\sqrt{3}`$| UNDEFINED|

## Standard Position and Co-terminal Angles

`Standard position` consists of 2 arms and an angle. The arm that is **ALWAYS** on the x-axis is called the `initial arm`. And the other arm is called the `terminal arm`.

Positive angles go counter-clockwise direction (to the right), and negative angles go clockwise direction (to the left).

`Co-terminal Angles` are angles whose terminal arms have the same standard position. Any 2 angles that are $`360^o`$ apart are considered `Co-terminal angles`.

## The CAST Rule

`Principal Angle` $`\theta`$: This is the angle usually given in the question. It is the counter-clockwise angle bewteen the initial arm and the terminal arm of an angle in standard position.

`Related Acute Angle or Reference Angle` ($`\alpha`$): The angle between the terminal arm and the **x-axis**. Note that this angle is always in the range $`0^o \le \alpha \le 90^o`$.

The CAST simply determines the positive/negatie signs of the result of a trig function of the related angle.

<img src="https://mathonline.wdfiles.com/local--files/cast-rule/Screen%20Shot%202013-11-23%20at%203.45.11%20PM.png" width="500">

Simple evaluate the related angle with the respective trigonmetery function, and add a negative sign according to the picture above.

## Solving Trigonmetric Equations

Just a few simple steps.

1. Simplifiy the expression to make all the trig functions on one side, and the constants on the other. Makes sure **not** to divide or omit trig functiosn involving the variable as you might be omitting solutions. 
2. Factor and simplify the expression, and state **ALL** possible solutions using `Co-terminal angles`.
3. Profit!

## Degrees and Radians

A few formulas:

To convert degrees to radians, multiply the degrees by $`\dfrac{\pi}{180^o}`$.

To convert radians to degrees, multiply the radians by $`\dfrac{180^o}{\pi}`$

To find the arc length ($`s`$) of the circle described by the angle and radius (or commonly known as **subtended** by the angle measure): $`s = r\theta`$, where $`\theta`$ is described in radians.

In the absence of the degree symbol, the angle must be assumed to be in radians. It is also useful to know that $`\pi = 180^o`$ and $`2\pi = 360^o`$

To find the RAA in terms of radians, follow the table below.

|Quadrant|Quadrant 1 (All)|Quadrant 2 (Sin)|Quadrant 3 (tan)| Quadrant 4(cos)|
|:-------|:---------------|:---------------|:---------------|:---------------|
|Step to do to get $`\alpha`$|$`\alpha = \theta`$|$`\alpha = \pi - \theta`$|$`\alpha = \theta - \pi`$|$`\alpha = 2\pi - \theta`$|

To find the area of a sector of a circle, the formula is as follows.

$`\text{Sector Area } = (\text{Area of Circle})(\% \text{ Of Circle shaded in})`$

$`A = (\pi r^2)(\dfrac{\theta}{2\pi})`$

$`A = \dfrac{1}{2}\theta r^2`$
