## Unit 4

Note: These notes are based off Sabrina's notes. A big thank you to her for providing me with these notes.

## Definitions
* `Amplitude`: Half the difference between **maximum** and **minimum** value of function.
- Amplitude $`= (max - min) / 2`$
- ALWAYS a positive value

* `Periodic`: A function is periodic if it has a pattern of cycles that repeats at regular intervals