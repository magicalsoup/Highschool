# Unit 2: Sequences, Series, and Financial Applications


## Terms
**Sequence**: is an ordered set of numbres.

**Arithmetic Sequence**: is a sequence where the difference between each term is constant, and the constant is known as the `common difference`.

**Geometric Sequence**: is a sequence in which the ratio between each term is constant, and the constant is known as the `common ratio`.

**Note:** Not all sequences are arithmetic and geometric!

**Finite Series**: finite series have a **finite** number of terms.
- eg. $`1 + 2 + 3 + \cdots + 10`$.

**Infinite Series**: infinite series have **infinite** number of terms.
- eg. $`1 + 2 + 3 + \cdots`$

Terms in a sequence are numbered with subscripts: $`t_1, t_2, t_3, \cdots t_n`$ where $`t_n`$is the general or $`n^{th}`$ term.

**Series**: A series is the sum of the terms of a sequence.


## Recursion Formula

A sequence is defined recursively if you have to calculate a term in a sequence from previous terms. The recursion formula consist of 2 parts.

1. Base term(s)
2. A formula to calculate each successive term.

eg. $`t_1 = 1, t_n = t_{n-1} + 1 \text{ for } n \gt 1`$

## Arithmetic Sequences

Basically, you add the **common difference** to the current term to get the next term. As such, it follows the following pattern:

$`a, a+d, a+2d, a+3d, a+4d, \cdots`$. Where $`a`$ is the first term and $`d`$ is the **common difference**.

As such, the general term of the arithmetic sequence is:

$`\large t_n = a + (n - 1)d`$

## Geometric Sequences

Multiply by the **common ratio** with the current term to get the next term. As such, it follows the following pattern:

$`a, ar, ar^2, ar^3, ar^4, c\dots`$. Where $`a`$ is the first term and $`r`$ is the **common ratio**.

As such, the general term of the geometric sequence is:

$`\large t_n = a(r)^{n-1}`$

## Arithmetic Series

An arithmetic series is the sum of the aritmetic sequence's terms.

The formula to calculate is:

$`\large S_n = \dfrac{n(a_1 + a_n)}{2}`$ Or $`\large S_n = \dfrac{n(2a_1 + (n-1)d)}{2}`$


## Geometric Series
- A geometric series is created by adding the terms of the geometric sequence.

The formula to calculate the series is:

$`\large S_n= \dfrac{a(r^n- 1)}{r-1}`$ or $`\large S_n = \dfrac{a(1 - r^n)}{1 - r}`$


## Series and Sigma Notation

It's often convenient to write summation of sequences using sigma notation. In Greek, sigma means to sum.

eg. $`S_ = u_1 + u_2 + u_3 + u_4 + \cdots + u_n = \sum_{i=1}^{n}u_i`$

$`\sum_{i=1}^{n}u_i`$ means to add all the terms of $`u_i`$ from $`i=1`$ to $`i=n`$.

Programmers might refer to this as the `for` loop.

```java
int sum=0;
for(int i=1; i<=N; i++) {
    sum += u[i];
}
```

## Infinite Geometric Series

Either the series **converges** and **diverges**. There is only a finite sum when the series **converges**.

Recall the our formula is $`\dfrac{a(r^n-1)}{r-1}`$, and is $`n`$ approaches $`\infty`$, if $`r`$ is less than $`1`$, then $`r^n`$ approaches $`0`$. So this
series converges. Otherwise, $`r^n`$ goes to $`\infty`$, so the series diverges.

If the series diverges, then the sum can be calculated by the following formula:

If $`r = \dfrac{1}{2}`$, then $`\large \lim_{x \to \infty} (\frac{1}{2})^x = 0`$ Therefore, $`S_n = \dfrac{a(1 - 0)}{1 - r}`$. This works for any $`|r| \lt 1`$

## Binomial Expansion
A binomial is a polynomial expression with 2 terms.

A binomial expansion takes the form of $`(x + y)^n`$, where $`n`$ is an integer and $`x, y`$ can be any number we want.

A common relationship of binomial expansion is Pascal's triangle. The $`nth`$ row of the triangle correspond to the coefficents of $`(x + y)^n`$

```
            1           row 0
           1 1          row 1
          1 2 1         row 2
         1 3 3 1        row 3
        1 4 6 4 1       row 4
      1 5 10 10 5 1     row 5        
```

The generalized version form of the binomial expansion is:

$`\large (x+y)^n = \binom{n}{0}x^ny^0 + \binom{n}{1} x^{n-1}y^1 + \binom{n}{2}x^{n-2}y^2 + \cdots+ \binom{n}{n-1}x^{n-(n-1)}y^{n-1} + \binom{n}{0} x^0y^n`$.

Written in sigma notation, it is:

$`\large (x+y)^n = \sum_{k=0}^{n} \binom{n}{k}x^ky^{n-k}`$

eg. $`\large(a + b)^3 = a^3 + 3a^2b + 3ab^2 + b^3`$

## Simple Interest

$`\large I = Prt`$

- $`P`$ is the principal amount (start amount of $)
- $`r`$ is the annual interest rate expressed as a decimal (the percent is $`1 - r`$)
- $`t`$ is the time in years.

- This interest is calculated from the original amount each time. (eg. if you had $100, and your interest is 1\%, your interest will be a constant $1 each time.)


The total amount would be $`P + I`$.

## Compound Interest

Compound interest is interest paid on the interest previously earned and the original investment. 

```math
\large A = P(1 + \frac{r}{n})^{nt}
```

- $`P`$ is the original amount
- $`\frac{r}{n} = i`$: this is the rate of interest **per period**.
    - $`r`$ is interest rate
    - $`n`$ is the number of periods (described below)
- $`nt`$ is the number of **total** periods (described below) Specifically, $`t`$ is the number of years. 
- $`A`$ is the total value of the investment after $`nt`$ investemnt periods.

|Compounding Period|$`n`$|$`nt`$|
|:-----------------|:----|:-----|
|Annual|$`n = 1`$|$`nt = t`$|
|Semi-annual|$`n = 2`$|$`nt = 2t`$|
|Quarterly|$`n = 4`$|$`nt = 4t`$|
|Monthly|$`n = 12`$|$`nt = 12t`$|
|Daily|$`n = 365`$|$`nt = 365t`$|

## Future Value Annuities
**Definition:** An annuity is a series of equal deposits made at equal time intervals. Each deposit is made at the end of each time interval.

A `Future Value` usually refers to how much money you will earn in the **future**. (eg. I have $100, I make deposits of $50 dollars each year with interest, how much will I have after $`5`$ years?)

Since it is the summation of a geometric sequence, we can apply the geometric series formula to get the following formula for future annuities:

```math
\large
FV = \frac{R[(1+\frac{r}{n})^{nt} - 1]}{\frac{r}{n}}
```

## Present Value Annuities
The **Present Value** of an annuity is today's value of having equally spaced payments or withdrawals of money sometime in the future.

```math
\large
PV = \frac{R[1 -(1+\frac{r}{n})^{-nt}]}{\frac{r}{n}}
```