# Unit 1: Exponential and Logarithmic Functions

## Review

`Function`: A relation where each x-value maps to exactly one y-value.

If given a function in the form $`y = af[k(x-d)] + c`$, then let $`(x,y)`$ be the original points, the new points will be $`(\dfrac{1}{k}x+d, ay+c)`$.


The domain and range of the exponential function is:
- $`D : \{x | x \in \mathbb{R}\}`$
- $`R : \{y | y > 0, y \in \mathbb{R}\}`$

The domain and range of the logarithmic function is
- $`D: \{x | x > 0, x \in \mathbb{R}\}`$
- $`R: \{y | y \in \mathbb{R}\}`$

If $`f(x)`$ is a function, then the inverse is $`f^{-1}(x)`$. The inverse has the following properties:
- Domain of $`f(x)`$ = Range of $`f^{-1}(x)`$
- Range of $`f(x)`$ = Domain of $`f^{-1}(x)`$

Graphically, the inverse of a function is by reflecting the original function over the line $`y=x`$.

A vertical line test is used to test whether a relation is a function. If any 2 points can be drawn through a vertical line, then that relation is **not a function**.

To solve/find the inverse of a function, just swap the $`y`$ and $`x`$ and isolate/solve for $`y`$.

## Exponential Decay/Growth
When the base ($`b`$) is in the range $`0 \lt b \lt 1`$, the exponential function is said to have a **exponential decay**, the smaller the base, the stronger the decay.

When the base ($`b`$) is in the range $`b \gt 1`$, the exponential function is said to have a **exponential growth**, the bigger the base, the stronger the growth.

## Graphing Exponential Functions
If you have **exponential growth** (meaning your base is greater than $`1`$), use more positive values rather than negative values.

If you have **exponential decay** (meaning your base is in the range $`(0, 1)`$), use more negative values rather than positive values.

**Don't forget the asymtote.**

## Logarithmic Function
The logarithmic function is the **inverse** of the exponential function.

In essence, if $`x = b^y`$, then $`\log_b x = y`$

**Note:** The logarithm is defined only for $`b > 0, b \ne 1`$

**Note 2:** The symbol $`ln`$ is $`log_e`$, we usually call it the **natural log**.

## Logrithm Laws

1. $`\log_b(b^x) = x`$
2. $`b^{\log_b(x)} = x`$
3. $`\log_b(1) = 0`$
4. $`\log_b(b) = 1`$


|Law|Form|Example|
|:--|:---|:------|
|Change Of Base (COB)|$`\log_a(b) = \dfrac{\log_m(b)}{\log_m(a)}`$|$`\log_2(5) = \dfrac{\log_{10}(5)}{\log_{10}(2)}`$|
|Change Of Base (COB)|$`\log_a(b) = \dfrac{1}{\log_b(a)}`$|$`\log_2(5) = \dfrac{1}{\log_5(2)}`$|
|Power Law|$`\log_b^m(x^n) = \dfrac{n}{m}\log_b(x)`$|$`\log_{2^2}(4^3) = \dfrac{3}{2}\log_2(4)`$|
|Product Law|$`\log_b(xy) = \log_b(x) + log_b(y)`$|$`\log_2(2 \times 3) = \log_2(2) + \log_2(3)`$|
|Quotient Law|$`\log_b(\dfrac{x}{y}) = \log_b(x) - \log_b(y)`$|$`log_2(\dfrac{2}{3}) = \log_2(2) - \log_2(3)`$

## Solving Logarithms

1. Using a common base and equating the the 2 exponents to one another ($`2^x = 4^{x-5} \implies 2^x = 2^{2x-10} \implies x = 2x-10`$)
2. Using a log rule to simplify and bring the exponenets to the "living room/main floor".

## Application of Exponential Growth

The formula for **Exponential Growth** is given as:

```math
\LARGE

N = N_0(R)^{\frac{t}{d}}

```

$`N = `$ Final amount

$`N_0 = `$ Starting amount

$`R =`$ Growth factor
- $`R = 1 + r`$
- **half-life:** $`R = \dfrac{1}{2}`$
- **doubling time:** $`R = 2`$

**Growth Rate**
- $`r > 0`$: Exponential growth
- $`-1 \lt r \lt 0`$: Exponential decay
- r is usually given as a percentage ($`\%`$)

$`t = `$: Total time for $`N_0`$ to get to $`N`$

$`d = `$ Time for 1 growth rate to occur
