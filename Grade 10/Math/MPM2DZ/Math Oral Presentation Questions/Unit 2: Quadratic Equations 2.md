## Quadratic Equations 2

### Question 4 a)

$`5x(x-1) + 5 = 7 + x(1-2x)`$

$`5x^2 - 5x = 2 + x - 2x^2`$

$`7x^2 - 6x - 2 = 0`$

$`x = \dfrac{6 \pm \sqrt{92}}{14} = \dfrac{3 \pm \sqrt{23}}{7}`$

### Question 4 b).

$`\because \dfrac{1}{3}`$ and $`\dfrac{-2}{3}`$ are the roots of a quadratic equation, that must mean that $`(x-\dfrac{1}{3})(x+\dfrac{2}{3})`$ is a quadratic equation that 
gives those roots. Here we make $`a=1`$, so its easy to find a quadratic in vertex from that gives these roots.

Let the vertex form then be $`y=(x-d)^2+c`$, since $`a=1`$.

We know $`d=\dfrac{r_1+r_2}{2}`$ because it is the x-coordinate of the vertex which is also the AOS. Therefore it is equal to $`\dfrac{\dfrac{1}{3} + \dfrac{-2}{3}}{2} = \dfrac{-1}{6}`$

Then, we know $`c=(d-\dfrac{1}{3})(d+\dfrac{2}{3})`$, since by plugging in the x-coordinate of the vertex, we get the y-coordinate of the vertex which is also the $`c`$ value.

Therefore $`c=(\dfrac{-1}{6}-\dfrac{1}{3})(\dfrac{-1}{6} + \dfrac{2}{3}) = \dfrac{-1}{4}`$.

Therefore our equation is simply $`y = (x+\dfrac{1}{6})^2 - \dfrac{1}{4}`$


### Qustion 4 c)

When $`h = 0`$, the ball hits ground, so:

$`-3.2t^2 + 12.8 + 1 = 0`$

$`t = \dfrac{12.8 \pm \sqrt{176.64}}{6.4}`$

$`\because t \ge 0`$


$`\therefore t \approx 4.1`$

The ball will strike the ground at approximately $`4.1`$ seconds.

### Question 5 a)

$`128 = 96t - 16t^2`$

$`16t^2 - 96t + 128 = 0`$

$`t^2 - 6t + 8 = 0`$

$`(t-2)(t-4)`$, at seconds $`2`$ and $`4`$, the rocket reaches $`128m`$.

### Question 5 b)

Break even is when revenue = cost.

$`\therefore R(d) = C(d)`$

$`-40d^2 + 200d = 300 - 40d`$

$`40d^2 - 240d + 300 = 0`$

$`2d^2 - 12d + 15 = 0`$

$`d = \dfrac{12 \pm \sqrt{24}}{4}`$

$`d = \dfrac{6 \pm \sqrt{6}}{2}`$

At $`d = 4.22474407`$ or $`1.775255135`$ is when you break even.

### Question 5 c)

Let the quation be $`y = a(x-d)^2 + c`$

Since we know that that $`(0, 0)`$ and $`(6, 0)`$ are the roots of this equation, the AOS is when $`x = 3`$

$`\therefore y = a(x-3)^2 + c`$.

Since we know that $`(0, 0)`$ is a point on the parabola, we can susbsitute it into our equation.

$`0 = 9a + c \quad (1)`$

Since we know that $`(4, 5)`$ is also a point on the parabola, we can susbsitute it int our equation as well.

$`5 = a + c \quad (2)`$

```math
\begin{cases}
9a + c = 0 & \text{(1)} \\
a + c = 5 & \text{(2)} \\
\end{cases}
```

$`(2) - (1)`$

$`-8a = 5 \implies a = \dfrac{-5}{8} \quad (3)`$

Sub $`3`$ into $`(2)`$:

$`5 = \dfrac{-5}{8} + c \implies c = \dfrac{45}{8}`$

$`\therefore`$ Our equation is $`y = \dfrac{-5}{8}(x-3)^2 + \dfrac{45}{8}`$