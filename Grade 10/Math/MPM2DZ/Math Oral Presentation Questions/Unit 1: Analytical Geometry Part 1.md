## Analytical Geometry Part 1

### Question 1 a)

Lets first find each of the side lengths to determine if the triangle is **obtuse**, **acute** or scalene.

$`\overline{AB} = \sqrt{(-1-7)^2 + (5-2)^2} = \sqrt{64 + 9} = \sqrt{73}`$

$`\overline{BC} = \sqrt{(7-(-1))^2 + (2-(-4))^2} = \sqrt{64 + 36} = \sqrt{100} = 10`$

$`\overline{AC} = \sqrt{(-1-(-1))^2 + (5-(-4))^2} = \sqrt{0^2 + 9^2} = \sqrt{81} = 9`$

$`\because \overline{AB} =\not \overline{BC} =\not \overline{AC}`$

$`\therefore \triangle ABC`$ is a scalene triangle.

### Question 1 b)

The `orthocenter` is the POI of the heights of a triangle.

$`m_{AB} = \dfrac{2-5}{7-(-1)} = \dfrac{-3}{8}`$

$`m_{\perp AB} = \dfrac{8}{3}`$

$`y_{\perp AB} - (-4) = \dfrac{8}{3}(x - (-1)) \implies y_{\perp AB} + 4 = \dfrac{8}{3}(x+1)`$

$`y_{\perp AB} = \dfrac{8}{3}x + \dfrac{8}{3} - 4`$

$` y_{\perp AB} = \dfrac{8}{3}x - \dfrac{4}{3} \quad (1)`$

$`m_{BC} = \dfrac{2-(-4)}{7-(-1)} = \dfrac{6}{8} = \dfrac{3}{4}`$

$`m_{\perp BC} = \dfrac{-4}{3}`$

$`y_{\perp BC} - 5 = \dfrac{-4}{3}(x-(-1)) \implies y_{\perp BC} - 5 = \dfrac{-4}{3}(x+1)`$

$`y_{\perp BC} = \dfrac{-4}{3}x - \dfrac{4}{3} + 5`$

$`y_{\perp BC} = \dfrac{-4}{3}x + \dfrac{11}{3}`$

```math

\begin{cases}

y_{\perp AB} = \dfrac{8}{3}x - \dfrac{4}{3} & \text{(1)} \\

\\ 
y_{\perp BC} = \dfrac{-4}{3}x + \dfrac{11}{3} & \text{(2)} \\
\end{cases}
```

Sub $`(1)`$ into $`(2)`$:

$`\dfrac{8}{3}x - \dfrac{4}{3} = \dfrac{-4}{3} + \dfrac{11}{3}`$

$`8x - 4 = -4x + 11`$

$`12x = 15`$

$`x = \dfrac{5}{4} \quad (3)`$

Sub $`(3)`$ into $`(2)`$

$`y = \dfrac{-20}{12} + \dfrac{11}{3}`$

$`y = \dfrac{-5}{3} + \dfrac{11}{3}`$

$`y = \dfrac{6}{3} = 2`$

$`y = 2`$

$`\therefore`$ The `orthocenter` is at $`(\dfrac{5}{4}, 2)`$

### Question 2 a)

midpoint = $` (\dfrac{\sqrt{72} + \sqrt{32}}{2}, \dfrac{-\sqrt{12} - \sqrt{48}}{2} )`$

$` = ( \dfrac{6\sqrt{2} + 4\sqrt{2}}{2}, \dfrac{-2\sqrt{3}, -4\sqrt{3}}{2}) `$

$` = 3\sqrt{2} + 2\sqrt{2}, -\sqrt{3} - 2\sqrt{3}`$

$` = (5\sqrt{2}, -3\sqrt{3})`$

$`\therefore`$ The midpoint is at $`(5 \sqrt{2}, -3\sqrt{3})`$

### Question 2 b)

Center of mass = centroid.

Centroid = where all median lines of a triangle intersect.

$`M_{AB} = (\dfrac{8+12}{2}, \dfrac{12+4}{2}) = (10, 8)`$

$`m_{M_{AB} C} = \dfrac{8-8}{10-2} = 0`$

$`y_{M_{AB} C} = 8 \quad (1)`$

$`M_{BC} = (\dfrac{12+2}{2}, {8+4}{2}) = (7, 6)`$

$`m_{M_{BC} A} = \dfrac{6-12}{7-8} = 6`$

$`y_{M_{BC}A} - 12 = 6(x-8)`$

$`y_{M_{BC}A} = 6x - 48 +12`$

$`y_{M_{BC}A} = 6x - 36 \quad (2)`$

```math
\begin{cases}

y_{M_{BC} A} = 8 & \text{(1)} \\

y_{M_{BC} A} = 6x - 36 & \text{(2)} \\

\end{cases}
```

Sub $`(1)`$ into $`(2)`$

$`8 = 6x - 36`$

$`6x = 44`$

$`x = \dfrac{44}{6} = \dfrac{22}{3} \quad (3)`$

By $`(1)`$, $`y=8`$.

$`\therefore`$ The centroid is at $`(\dfrac{22}{3}, 8)`$
