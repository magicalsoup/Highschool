## Quadratic Equations 1

### Question 1 a)

The zeroes of a quadratic equation are the solutions of $`x`$ when $`ax^2+bx+c = 0`$. The roots of the quadratic equation is when $`(x - r_1)(x - r_2) = 0`$, more 
commonly described by the formula $`\dfrac{-b \pm \sqrt{b^2-4ac}}{2a}`$. Therefore the roots of a quadratic equation are also the zeroes of the quadratic equation.

### Question 1 b)

$`D = b^2 - 4ac`$

If $`D=0`$, there is one zero.

$`\therefore n^2 - 4(1)(9) = 0`$

$`n^2 - 36 = 0`$

$`(n+6)(n-6) = 0`$

$`n = \pm 6`$

### Question 1 c)

Let $`h`$ be the height and $`b`$ be the base. $`h = 2b + 4`$.

$`\therefore (2b+4)(b) = 168(2)`$

$`2b^2 + 4b = 168(2)`$

$`b^2 + 2b - 168 = 0`$

$`b = \dfrac{-2 \pm \sqrt{4+4(168)}}{2}`$

$`b = \dfrac{-2 \pm 26}{2}`$

$`b = -1 \pm 13 \implies b = 12, -14`$

$`\because b \gt 0 `$

$`\therefore b = 12cm`$

### Question 2 a)

If the quadratic is in $`ax^2 + bx + c`$, the AOS (axis of symmetry) is at $`\dfrac{-b}{2a}`$. And you can plug that value into the quadratic equation to get your optimal value,
which is:

$`= a(\dfrac{-b}{2a})^2 + b(\dfrac{-b}{2a}) + c`$

$`= \dfrac{b^2}{4a} + \dfrac{-b^2}{2a} + c`$

$`= \dfrac{-b^2}{4a} + c`$

### Question 2 b)

$`2x^2 + 5x - 1 = 0`$

$`2(x^2 + \dfrac{5}{2}x + (\dfrac{5}{4})^2 - (\dfrac{5}{4})^2) - 1 = 0`$

$`2(x+\dfrac{5}{4})^2 - \dfrac{25}{8} - 1 = 0`$

$`2(x+ \dfrac{5}{4})^2 - \dfrac{33}{8} = 0`$

$`(x+ \dfrac{5}{4})^2 = \dfrac{33}{16}`$

$`x = \pm \sqrt{\dfrac{33}{16}} - \dfrac{5}{4}`$

$`x = \dfrac{\pm \sqrt{33}}{4} - \dfrac{5}{4}`$

$`x = \dfrac{\sqrt{33}-5}{4}`$ or $`\dfrac{-\sqrt{33} - 5}{4}`$

### Question 2 c)

Let $`w`$ be the width between the path and flowerbed, $`x`$ be the length of the whole rectangle and $`y`$ be the whole rectangle (flowerbed + path).

$`x = 9+2w`$

$`y = 6+2w`$

$`(6+2w)(9+2w) - (6)(9 = (6)(9)`$

$`54 + 12w + 18w + 4w^2 = 2(54)`$

$`4w^2 + 30w = 54`$

$`2w^2 + 15w - 27 = 0`$

$`(2w-3)(w+9) = 0`$

$`w = \dfrac{3}{2}, -9`$

$`\because w \gt 0`$

$`\therefore w = \dfrac{3}{2}`$

$`\therefore x = 9+3 = 12`$

$`\therefore y = 6+3 = 9`$

$`P = 2(x + y) \implies P = 2(12+9) \implies P = 42`$

$`\therefore`$ The perimeter is $`42m`$

### Question 3 a)

Use discriminant, where $`D = b^2 - 4ac`$.

```math
\begin{cases}

\text{If } D \gt 0 & \text{Then there are 2 real distinct solutions} \\

\text{If } D = 0 & \text{Then there is 1 real solution} \\

\text{If } D \lt 0 &\text{Then there are no real solutions} \\

\end{cases}
```

### Question 3 b)

$`y = 12x^2 - 5x - 2`$

$`y = (3x-2)(4x+1)`$

$`\therefore`$ The $`x`$-intercepts are at $`\dfrac{2}{3}, \dfrac{-1}{4}`$

### Question 3 c)

When $`P(x) = 0`$, that means it is the break-even point for a value of $`x`$ (no profit, no loss).

$`2k^2 + 12k - 10 = 0 \implies k^2 -6k + 5 = 0`$

$`(k-5)(k-1) = 0`$

$`k = 5, 1`$

Either $`5000`$ or $`1000`$ rings must be produced so that there is no prodift and no less.

AOS (axis of symmetry)  = $`\dfrac{-b}{2a} = \dfrac{6}{2} = 3`$ 

$`\therefore 3000`$ rings should be made to achieve the optimal value.

Maximum profit $`= -2(3)^2 + 12(3) - 10`$

$`= -18 + 30 - 10`$

$` = 8`$

$`\therefore 8000`$ dollars is the maximum profit.

