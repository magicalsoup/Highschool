# Quiz 1

**1. Evaluate** $`\dfrac{18 \div 3 + 2}{(2-6) \times 3}`$


- $`= \dfrac{6+2}{(-4) \times 3}`$
    
- $`= \dfrac{8}{-12}`$
    
- $`= \dfrac{-2}{3}`$
    
**2. Evaluate** $`(-3\dfrac{1}{12} + 2\dfrac{5}{8}) + \dfrac{12}{4} \times \dfrac{1}{6}`$

- $` = (\dfrac{-37}{12} + \dfrac{21}{8}) + \dfrac{2}{4} \times \dfrac{1}{1}`$
 
- $` = (\dfrac{-74}{24} + \dfrac{63}{24}) + \dfrac{2}{4}`$

- $` = \dfrac{-11}{24} + \dfrac{12}{24}`$

- $` = \dfrac{1}{24}`$

**3. Evaluate** $`(\dfrac{34x^2y^3z^6}{16x^{-2}y^5z^3})^{\small -2}`$

- $` = (\dfrac{17x^2y^3z^6}{8x^{-2}y^5z^3})^{\small -2}`$

- $` = (\dfrac{17x^4y^3z^6}{8y^5z^3})^{\small -2}`$ 

- $` = (\dfrac{17x^4z^3}{8y^2})^{\small -2}`$ 

- $` = \dfrac{17^{-2}x^{-8}z^{-6}}{8^{-2}y^{-4}}`$ 

- $` = \dfrac{8^2y^4}{17^2x^8z^6}`$

**4. Expand and Simplify** $`2(x+y)^2 - (x^2-2y^2)`$

- $` = 2(x^2+2xy+y^2) - x^2 + 2y^2`$

- $` = 2x^2 + 4xy + 2y^2 - x^2 + 2y^2`$

- $` = x^2 + 4xy + 4y^2`$ 