## Study Notes for Highschool
- These notes are created with a purpose to help people with their final exams, they are labeled by ```subject``` then the ```course code```, eg (```Math```, then ```MPM1DZ```)

## Important
- If there's any form of plagiarism, please contact me via my email or as an issue on this gitlab.
- For any teachers, if there's a thing that needs to be taken down, please talk to me.

Notes moved to: https://eifueo.eggworld.me/
